import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import browsable from '../browsable';
import utils from '../utils';
import Representation from '../base/Representation';
import Container from './Container';
declare var app: any;

type TextOptions = PIXI.TextStyleOptions & { text?: string };

export default class Text extends Container {
	static description: string = `Simple multiline text with font style, size and color.`;

	_text!: PIXI.Text;
	set text(value: PIXI.Text | string) {
		if (!this._text && value instanceof PIXI.Text) {
			this._text = value;
			this.addChild(this._text);
		} else if (typeof value == 'string') {
			this._text.text = value;
		}
	}
	get text() {
		return this._text;
	}

	set color(color) {
		this._text.style.fill = color;
	}

	constructor(name: string, protected repr: Representation, textOptions: TextOptions = {}) {
		super(name, repr);

		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);

		textOptions.align = textOptions.align ?? 'left';
		textOptions.wordWrap = textOptions.wordWrap ?? true;
		textOptions.fill = textOptions.fill ?? '#ffffff';
		textOptions.wordWrapWidth = repr.size[0];

		this.text = new PIXI.Text(textOptions.text, textOptions);
	}
}