import { Sprite } from 'pixi.js'
import rootDir from '../PathResolver';

export default function Image(path: string, width: number, height: number, scaleMode: number = PIXI.SCALE_MODES.LINEAR): Sprite {
	const sprite = PIXI.Sprite.fromImage(`${rootDir}/${path.replace('%locale%', 'ru')}`);
	sprite.texture.baseTexture.scaleMode = scaleMode;
	sprite.position.set(0, 0);
	sprite.width = width;
	sprite.height = height;
	return sprite;
}