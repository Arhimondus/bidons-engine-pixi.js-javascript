import Container from './Container';
import Text from './Text';
import Representation from '../base/Representation';
import Button from './Button';

enum ClickMode {
	Down = 'Down',
	Up = 'Up',
}

export default class Dropdown extends Container {
	clickMode: ClickMode = ClickMode.Down;
	isMouseDown: boolean = false;
	isMouseOver: boolean = false;

	originalY?: number = undefined;
	_selectedItem: any = null;
	set selectedItem(value) {
		this._selectedItem = value;
		this.textTitle = value.toString();
	}
	get selectedItem() {
		return this._selectedItem;
	}

	private _textTitle!: Text;
	buttonContainer: PIXI.Container;
	itemsContainer: PIXI.Container;

	originalTextTitlefontSize: number;
	set textTitle(value: string) {
		this._textTitle.text = value;
		if (value.length > 15) {
			this.textOptionsTitle.fontSize = this.originalTextTitlefontSize - 4;
			this._textTitle._text.style = new PIXI.TextStyle(this.textOptionsTitle);
		} else if (this._textHint._text.style.fontSize != this.originalTextTitlefontSize) {
			this.textOptionsTitle.fontSize = this.originalTextTitlefontSize;
			this._textTitle._text.style = new PIXI.TextStyle(this.textOptionsTitle);
		}
	}
	get textTitle() {
		return this._textTitle.text as string;
	}

	private _textHint!: Text;
	set textHint(value: string) {
		this._textHint.text = value;
	}
	get textHint() {
		return this._textHint.text as string;
	}

	constructor(
		name: string,
		repr: Representation,
		public textOptionsTitle: PIXI.TextStyleOptions,
		public textOptionsHint: PIXI.TextStyleOptions,
		public normalState: PIXI.Sprite,
		public downState: PIXI.Sprite,
		public itemTexture: PIXI.BaseTexture,
		public itemActiveTexture: PIXI.BaseTexture,
		public disableState?: PIXI.Sprite,
	) {
		super(name, repr);
		this.originalTextTitlefontSize = textOptionsTitle.fontSize as number;
		this.buttonContainer = new PIXI.Container();
		this.buttonContainer.let((it: any) => {
			it.interactive = true;
			it.buttonMode = true;

			it.mouseover = () => {
				this.isMouseOver = true;
			}

			it.mouseout = () => {
				this.isMouseOver = false;
			}

			it.mousedown = () => {
				this.isMouseDown = true;
				it.removeChild(this.normalState);
				it.addChildAt(this.downState, 0);
				if (this.clickMode == ClickMode.Down) {
					if (this.open) {
						this.open();
					}
				}
			}

			it.mouseup = () => {
				this.isMouseDown = false;
				this.isMouseOver = true;
				it.removeChild(this.downState);
				it.addChildAt(this.normalState, this.children.length - 1);
				it.setChildIndex(this._textHint, this.children.length - 1);
				it.setChildIndex(this._textTitle, this.children.length - 1);
				if (this.clickMode == ClickMode.Up) {
					if (this.open) {
						this.open();
					}
				}
			}

			it.mouseupoutside = () => {
				this.isMouseDown = false;
				this.isMouseOver = false;
				it.removeChild(this.downState);
				it.addChildAt(this.normalState, this.children.length - 1);
				it.setChildIndex(this._textHint, this.children.length - 1);
				it.setChildIndex(this._textTitle, this.children.length - 1);
				if (this.clickMode == ClickMode.Up) {
					if (this.open) {
						this.open();
					}
				}
			}
		})
		this.buttonContainer.addChild(this.normalState);

		let title = new Text('DropdownTitle', repr, textOptionsTitle);
		this.buttonContainer.addChild(title);
		title.let((t: Text) => {
			t._text.anchor.set(0.5, 0.5);
			t.position.set(this.repr.size[0] / 2, this.repr.size[1] / 2);
			t._text.visible = true;
			t.visible = true;
		});
		this._textTitle = title;

		let hint = new Text('DropdownHint', repr, textOptionsHint);
		this.buttonContainer.addChild(hint);
		hint.let((t: Text) => {
			t._text.anchor.set(0, 0);
			t.position.set(15, 0);
			t._text.visible = true;
			t.visible = true;
		});
		this._textHint = hint;

		this.addChild(this.buttonContainer);

		this.itemsContainer = new PIXI.Container();
		this.addChildAt(this.itemsContainer, 0);
	}

	items: any[] = [];
	isOpened = false;
	// openedChilds: Container[] = [];

	open() {
		if (this.isOpened) {
			// for (var child of this.openedChilds) {
			this.itemsContainer.removeChildren();
			// this.removeChild(child);
			// }
			this.isOpened = false;
			return;
		}
		this.isOpened = true;
		// let aChilds: Container[] = [];
		this.bringToFront();
		console.log(`opened ${this.items.length} elements`);
		let createItem = (item, index) => {
			let stringRepr = item.toString();
			let container: Button = new Button(stringRepr + '#' + index, {
				size: [this.repr.size[0], this.repr.size[1] - 5],
				anchor: [0, 0],
				location: [5, 38 + index * 40],
			}, { fontFamily: 'mr_StupidHeadBBG', fontSize: 18, fill: 'white' },
				PIXI.Sprite.from(this.itemTexture).also(s => { s.width = this.repr.size[0] - 8, s.height = this.repr.size[1] - 5 }),
				PIXI.Sprite.from(this.itemTexture).also(s => { s.width = this.repr.size[0] - 8, s.height = this.repr.size[1] - 5 }),
				PIXI.Sprite.from(this.itemActiveTexture).also(s => { s.width = this.repr.size[0] - 8, s.height = this.repr.size[1] - 5 })).also(c => {
					c.text = stringRepr;
				});
			container.click = () => {
				console.log('clicked!');
				if (item.click()) {
					this.selectedItem = item;
					this.itemsContainer.removeChildren();
					this.isOpened = false;
				}
			}
			return container;
		}
		for (var [index, item] of this.items.entries()) {
			let newChild = createItem(item, index);
			// aChilds.push(newChild);
			this.itemsContainer.addChild(newChild);
		}
		for (var dobj of this.itemsContainer.children.reverse()) {
			// (dobj as Container).bringToFront();
		}

		// try {
		// 	this.setChildIndex(this.downState, this.children.length - 1);
		// } catch (err) { };
		// try {
		// 	this.setChildIndex(this.normalState, this.children.length - 1);
		// } catch (err) { };
		// this.setChildIndex(this._textHint, this.children.length - 1);
		// this.setChildIndex(this._textTitle, this.children.length - 1);

		// this.openedChilds = aChilds;
	}
}