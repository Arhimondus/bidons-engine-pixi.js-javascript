import Overlay from './Overlay';
import Image from './Image';
import Animation from "./Animation";
import { Sprite } from 'pixi.js';

export default function SpriteDetector(path: string, width: number, height: number, scaleMode: number = PIXI.SCALE_MODES.LINEAR): Sprite {
	if (path.endsWith('_overlay.apng')) {
		return Overlay(path, width, height, scaleMode);
	} else if (path.endsWith('.apng') || path.endsWith('.gif')) {
		return Animation(path, width, height, scaleMode);
	} else {
		return Image(path, width, height, scaleMode);
	}
}