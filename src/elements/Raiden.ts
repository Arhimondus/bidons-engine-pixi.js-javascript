import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import Representation from '../base/Representation';
import Container from './Container';
import Animation from './Animation';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';

export default class Raiden extends PIXI.Container {
	static description: string = `Raiden animation container.`;

	get size(): PIXI.Point {
		return new PIXI.Point(...this.repr.size);
	}

	anchor: PIXI.ObservablePoint = new PIXI.ObservablePoint(() => {
		const pivotX = this.anchor.x * this.size.x;
		const pivotY = this.anchor.y * this.size.y;
		console.log(pivotX, pivotY);
		this.pivot.set(pivotX, pivotY);
	}, this, 0, 0);
	
	callSprite: PIXI.Sprite;
	awaySprite: PIXI.Sprite;
	
	call() {
		this.visible = true;
		this.callSprite.jumpToFrame(0);
		this.callSprite.visible = true;
		this.callSprite.play(1);
		BdlfContext.instance.play('audio/lighting.wav');
	}
	
	away() {
		this.visible = true;
		this.awaySprite.jumpToFrame(0);
		this.callSprite.visible = false;
		this.awaySprite.visible = true;
		this.awaySprite.play(1);
		BdlfContext.instance.play('audio/lighting.wav');
	}

	constructor(public repr: Representation) {
		super();

		this.visible = repr.visible ?? true;
		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);

		this.callSprite = Animation('assets/sprites/Raiden_come.apng', 1280, 720).also(it => {
			it.stop();
			it.visible = false;
			it.on('played', (abc, def) => {
				it.jumpToFrame(it.getFramesLength() - 1);
			});
			this.addChild(it);
		});
		
		this.awaySprite = Animation('assets/sprites/Raiden_gone.apng', 1280, 720).also(it => {
			it.stop();
			it.visible = false;
			it.on('played', (abc, def) => {
				it.jumpToFrame(it.getFramesLength() - 1);
			});
			this.addChild(it);
		});
	}
}