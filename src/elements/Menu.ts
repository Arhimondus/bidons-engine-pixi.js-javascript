import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import Representation from '../base/Representation';
import Container from './Container';
import Interactive from '../base/Interactive';
import BdlfContext from '../bdlf';
import rootDir from '../PathResolver';

type InteractiveContainer = Container & Interactive;

export default class Menu extends Container {
	static description: string = `Menu elements.`;
	menumode: string;

	createItem(item: any, context: any, index: number) {
		return new Container(`menuItem${index}`, {
			size: [370, 50],
			anchor: [0, 0],
			location: [0, index * 53],
			backgroundColor: '#ba4488',
		}).also((c: InteractiveContainer) => {
			c.interactive = true;
			c.alpha = 0.8;
			let title = typeof(item.title) == 'string' ? item.title : item.title(this.bdlfContext);
			title = title.replace(/::dev{(.*?)}/g, process.env.NODE_ENV == 'development' ? '|$1|' : '');
			c.addChild(new PIXI.Text(title, {
				fontFamily: 'Arial',
				fontSize: 16,
				fill: 'white',
				align: 'left',
				wordWrapWidth: 370,
			}).also((text: PIXI.Text) => {
				text.position.set(20, 25);
				text.anchor.set(0, 0.5);
			}));
			c.mousedown = (event) => {
				this.visible = false;
				this.bdlfContext.activeMenu = undefined;
				this.bdlfContext.overlay();
				const menuResult = item.action(context.bdlf, context.scene, context.game, context.engine);
				console.log(`menuResult default ${menuResult}`);
				if (typeof (menuResult) == 'number') {
					this.bdlfContext.run(menuResult);		
				}
				event.stopPropagation();
			}
		});
	}

	createIconItem(item: any, context: any, index: number) {
		var container = new PIXI.Container() as InteractiveContainer;

		let size = 180;
		let [ pletka, id ] = item.title.slice(1).split('|');
		let title = this.bdlfContext.byId(id);

		var backcolor = PIXI.Sprite.fromImage(`${rootDir}/assets/pletki/${pletka}.png`) as PIXI.Sprite;
		backcolor.width = size;
		backcolor.height = size;
		// backcolor.tint = h2d.hexToDec('ba4488');
		backcolor.visible = true;
		container.addChild(backcolor);

		var text = new PIXI.Text(title, {
			fontFamily: 'Arial',
			fontSize: 16,
			fill: 'white',
			align: 'left',
			wordWrapWidth: 370,
		});
		text.position.set(size / 2, size);
		text.anchor.set(0.5, 1);
		container.addChild(text);

		container.interactive = true;
		container.mousedown = (event) => {
			this.visible = false;
			this.bdlfContext.activeMenu = undefined;
			this.bdlfContext.overlay();
			const menuResult = item.action(context.bdlf, context.scene, context.game, context.engine);
			console.log(`menuResult default iconed ${menuResult}`);
			if (typeof (menuResult) == 'number') {
				this.bdlfContext.run(menuResult);
			}
			event.stopPropagation();
		}
		container.position.x = 150 + index * 235;

		container.alpha = 0.8;

		return container;
	}

	changeMenu(menu: any, context: any /* bdlf, scene, game, engine */) {
		try {
			this.removeChildren(1);
		} catch (err) { }

		this.repr.size = [370, menu.length * 53];
		this.anchor.set(...this.repr.anchor);

		if (this.menumode != 'overlay') {
			for (var [index, item] of menu.entries()) {
				if (item.condition) {
					if (!item.condition(context.bdlf, context.scene, context.game, context.engine)) {
						continue;
					}
				}
				if (this.menumode == 'default') {
					this.addChild(this.createItem(item, context, index));
				} else if (this.menumode == 'overlay') {
					// Do nothing
				} else {
					this.addChild(this.createIconItem(item, context, index));
				}
			}
			this.visible = true;
		} else {
			this.visible = false;
		}
	}

	changeMenuMode(menumode: string) {
		this.menumode = menumode;
	}

	constructor(name: string, protected repr: Representation, public bdlfContext: BdlfContext) {
		super(name, repr);

		this.menumode = 'default';
		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);
		this.visible = false;
	}
}