import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import Representation from '../base/Representation';
import Container from './Container';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';

export default class Sprite extends Container {
	static description: string = `Base sprite class.`;

	// anchor: PIXI.ObservablePoint = new PIXI.ObservablePoint(() => {
	// 	const pivotX = this.anchor.x * this.size.x;
	// 	const pivotY = this.anchor.y * this.size.y;
	// 	console.log(pivotX, pivotY);
	// 	this.pivot.set(pivotX, pivotY);
	// }, this, 0, 0);

	// get size(): PIXI.Point {
	// 	return new PIXI.Point(...this.repr.size);
	// }

	private get bdlfSprite() {
		if (!this.bdlfContext.contextVars.sprites) {
			this.bdlfContext.contextVars.sprites = {};
		}
		if (!this.bdlfContext.contextVars.sprites[this.spriteName]) {
			this.bdlfContext.contextVars.sprites[this.spriteName] = { base: this.spriteName, modifiers: {} };
		}
		if (!this.bdlfContext.contextVars.sprites[this.spriteName].modifiers) {
			this.bdlfContext.contextVars.sprites[this.spriteName].modifiers = {};
		}
		return this.bdlfContext.contextVars.sprites[this.spriteName];
	}

	protected setSlot(slot, value) {
		if (!value) {
			value = this.slots[slot].default1;
		}

		this['_' + slot].texture = PIXI.Texture.from(this.slots[slot].path.replace('[slotName]', value));
		this['_' + slot].slotName = slot;

		// debugger;

		this.bdlfContext.modify(this.spriteName, {
			...this.bdlfSprite.modifiers,
			[slot]: value,
		});
	}

	protected image_zoom(image: any, texture: any, size: [number, number]) {
		var sc = 1, ox = 0, oy = 0;

		var width = texture.width;
		var height = texture.height;

		if (height > size[1]) {
			sc = size[1] / height;
		}

		if (width > size[0]) {
			var new_sc = size[0] / width;
			if (new_sc < sc) sc = new_sc;
		}

		width = texture.width * sc;
		height = texture.height * sc;

		// Выровнять по центру по высоте
		if (height < size[1]) {
			oy = (size[1] - height) / 2;
		} else {
			oy = 0;
		}

		// Выровнять по центру по ширине
		if (width < size[0]) {
			ox = (size[0] - width) / 2;
		} else {
			ox = 0;
		}

		image.scale.set(sc, sc);
		image.position.set(ox, oy);
	}

	constructor(protected spriteName, protected repr, protected bdlfContext, protected slots) {
		// super();
		super(spriteName, repr);

		this.visible = false;

		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);

		Object.keys(slots).map(slot => {
			console.log(spriteName, slot);
			const { path, default1 } = slots[slot];
			const slotField = this['_' + slot] = PIXI.Sprite.fromImage(path.replace('[slotName]', default1));
			slotField.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
			slotField.slotName = slot;
			if (!slotField.texture.baseTexture.isLoading) {
				this.image_zoom(slotField, slotField.texture, this.repr.size);
			} else {
				slotField.texture.baseTexture.on('loaded', () => {
					this.image_zoom(slotField, slotField.texture, this.repr.size);
				});
			}
			this.addChild(slotField);
		});
	}
}