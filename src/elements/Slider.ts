import Container from './Container';
import * as PIXI from 'pixi.js';
import Representation from '../base/Representation';

export default class Slider extends Container {
	constructor(name: string, repr: Representation, public gripSprite: PIXI.Sprite, public backgroundSprite: PIXI.Sprite) {
		super(name, repr);
		this.addChild(backgroundSprite);
		this.addChild(gripSprite);
	}
}