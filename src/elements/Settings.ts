import '../extensions';
import Container from './Container';
import { remote } from 'electron';
import Button from './Button';
import Dropdown from './Dropdown';
import Slider from './Slider';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';
import AppSettings from '../base/AppSettings';


export default class Settings extends Container {
	static description: string = `Default settings scene.`;

	settings: any;
	languageDropdown: Dropdown;
	screenModeDropdown: Dropdown;
	skipSlider: Slider;
	volumeSlider: Slider;

	resetButton: Button;
	backButton: Button;

	constructor(public bdlfContext: BdlfContext) {
		super('Settings', {
			size: [1280, 720],
			location: [0, 0],
			anchor: [0, 0],
		});

		let shakaledSepia = PIXI.Sprite.from(`${rootDir}/assets/backgrounds/shakaled_sepia.jpg`);
		this.addChild(shakaledSepia);

		let sqNormalState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_normalState.png`);
		let sqHoverState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_hoverState.png`);
		let sqDownState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_downState.png`);

		let newSqNormalState = () => PIXI.Sprite.from(sqNormalState).also((sprite: PIXI.Sprite) => {
			sprite.width = 250;
			sprite.height = 60;
		});
		let newSqHoverState = () => PIXI.Sprite.from(sqHoverState).also((sprite: PIXI.Sprite) => {
			sprite.width = 250;
			sprite.height = 60;
		});
		let newSqDownState = () => PIXI.Sprite.from(sqDownState).also((sprite: PIXI.Sprite) => {
			sprite.width = 250;
			sprite.height = 60;
		});

		let sqtextOptions: PIXI.TextStyleOptions = { fontFamily: 'mr_StupidHeadBBG', fontSize: 24, fill: 'white' };

		this.resetButton = new Button('ResetButton', {
			size: [250, 60],
			anchor: [0, 0],
			location: [762, 648],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.text = this.bdlfContext.igt('st_reset');
			it.visible = false;
			this.addChild(it);
		});

		this.backButton = new Button('BackButton', {
			size: [250, 60],
			anchor: [0, 0],
			location: [1018, 648],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.text = this.bdlfContext.igt('st_back');
			it.click = () => {
				this.bdlfContext.engine.application.popScene();
				this.bdlfContext.run(this.bdlfContext.dialog('main'));
			}
			this.addChild(it);
		});

		let ddNormalState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/dropDown_normalState.png`);
		let ddDownState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/dropDown_downState.png`);
		let ddItem = PIXI.BaseTexture.from(`${rootDir}/assets/ui/dropDown_listItem.png`);
		let ddItemActive = PIXI.BaseTexture.from(`${rootDir}/assets/ui/dropDown_listItemActive.png`);

		let newDdNormalState = () => PIXI.Sprite.from(ddNormalState).also((sprite: PIXI.Sprite) => {
			sprite.width = 250;
			sprite.height = 60;
		});
		let newDdDownState = () => PIXI.Sprite.from(ddDownState).also((sprite: PIXI.Sprite) => {
			sprite.width = 250;
			sprite.height = 60;
		});

		let ddtextOptionsHint: PIXI.TextStyleOptions = { fontFamily: 'mr_StupidHeadBBG', fontSize: 14, fill: 'white' };
		let ddtextOptionsTitle: PIXI.TextStyleOptions = { fontFamily: 'mr_StupidHeadBBG', fontSize: 24, fill: 'white' };

		let tm = this.bdlfContext.engine.transManager;
		this.languageDropdown = new Dropdown('LanguageDropdown', {
			size: [250, 60],
			anchor: [0, 0],
			location: [347, 276],
		}, ddtextOptionsTitle, ddtextOptionsHint, newDdNormalState(), newDdDownState(), ddItem, ddItemActive).also(it => {
			it.textHint = this.bdlfContext.igt('st_language');
			it.items = [
				{
					id: 'ru', name: 'Русский', toString() { return this.name; }, appSettings: this.bdlfContext.engine.appSettings,
					click() {
						tm.changeLanguage(this.id);
						this.appSettings.current.language = this.id;
						this.appSettings.saveSettings();
						return true;
					},
				},
				{
					id: 'en', name: '€nglish', toString() { return this.name; }, appSettings: this.bdlfContext.engine.appSettings,
					click() {
						tm.changeLanguage(this.id);
						this.appSettings.current.language = this.id;
						this.appSettings.saveSettings();
						return true;
					},
				},
				// {
				// 	id: 'de', name: 'Deutsch', toString() { return this.name; },
				// 	click() {
				// 		return true;
				// 	},
				// },
				// {
				// 	id: 'zh', name: '中文', toString() { return this.name; },
				// 	click() {
				// 		return true;
				// 	},
				// },
				// {
				// 	id: 'pl', name: 'Polski', toString() { return this.name; },
				// 	click() {
				// 		return true;
				// 	},
				// },
			];
			it.selectedItem = it.items.find(f => f.id == this.bdlfContext.engine.transManager.currentLang);
			this.addChild(it);
		});

		this.screenModeDropdown = new Dropdown('ScreenModeDropdown', {
			size: [250, 60],
			anchor: [0, 0],
			location: [347, 356],
		}, ddtextOptionsTitle, ddtextOptionsHint, newDdNormalState(), newDdDownState(), ddItem, ddItemActive).also(it => {
			it.textHint = this.bdlfContext.igt('st_screenmode');
			it.items = [
				{
					id: 'window', name: this.bdlfContext.igt('st_windowmode'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						remote.getCurrentWindow().let((it: Electron.BrowserWindow) => {
							it.setFullScreen(false);
						});
						return true;
					},
				},
				{
					id: 'fullscreen', name: this.bdlfContext.igt('st_fullscreen'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						remote.getCurrentWindow().let((it: Electron.BrowserWindow) => {
							it.setFullScreen(true);
						});
						return true;
					},
				},
			];
			it.selectedItem = remote.getCurrentWindow().isFullScreen() ? it.items[1] : it.items[0];
			this.addChild(it);
		});

		new Dropdown('ReplicasSkipSpeedDropdown', {
			size: [250, 60],
			anchor: [0, 0],
			location: [747, 276],
		}, ddtextOptionsTitle, ddtextOptionsHint, newDdNormalState(), newDdDownState(), ddItem, ddItemActive).also((it: Dropdown) => {
			it.textHint = this.bdlfContext.igt('st_replicasskipspeed');
			it.items = [
				{
					id: 'slow', name: this.bdlfContext.igt('st_slow'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						this.appSettings.current.skipSpeed = 'slow';
						this.appSettings.saveSettings();
						return true;
					},
				},
				{
					id: 'medium', name: this.bdlfContext.igt('st_medium'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						this.appSettings.current.skipSpeed = 'medium';
						this.appSettings.saveSettings();
						return true;
					},
				},
				{
					id: 'fast', name: this.bdlfContext.igt('st_fast'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						this.appSettings.current.skipSpeed = 'fast';
						this.appSettings.saveSettings();
						return true;
					},
				},
			];
			it.selectedItem = it.items.find(f => f.id == this.bdlfContext.engine.appSettings.current.skipSpeed);
			this.addChild(it);
		});

		new Dropdown('CharacterColorDropdown', {
			size: [250, 60],
			anchor: [0, 0],
			location: [747, 356],
		}, ddtextOptionsTitle, ddtextOptionsHint, newDdNormalState(), newDdDownState(), ddItem, ddItemActive).also((it: Dropdown) => {
			it.textHint = this.bdlfContext.igt('st_characterscolor');
			it.items = [
				{
					id: 'colored', name: this.bdlfContext.igt('st_colored'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						this.appSettings.current.coloredCharacters = true;
						this.appSettings.saveSettings();
						return true;
					},
				},
				{
					id: 'uncolored', name: this.bdlfContext.igt('st_uncolored'), appSettings: this.bdlfContext.engine.appSettings,
					toString() { return this.name; },
					click() {
						this.appSettings.current.coloredCharacters = false;
						this.appSettings.saveSettings();
						return true;
					},
				},
			];
			it.selectedItem = this.bdlfContext.engine.appSettings.current.coloredCharacters ? it.items[0] : it.items[1];
			this.addChild(it);
		});

		let ssGrip = PIXI.Sprite.from(`${rootDir}/assets/ui/squaredButton_hoverState.png`);
		let ssBackground = PIXI.Sprite.from(`${rootDir}/assets/ui/squaredButton_downState.png`);

		this.skipSlider = new Slider('SkipSlider', {
			size: [250, 60],
			anchor: [0, 0],
			location: [0, 0],
		}, ssGrip, ssBackground).also(it => {
			it.gripSprite.position.set(75, 15);
			this.addChild(it);
		});

		this.volumeSlider = new Slider('VolumeSlider', {
			size: [250, 60],
			anchor: [0, 0],
			location: [0, 0],
			visible: false,
		}, ssGrip, ssBackground).also(it => {
			it.gripSprite.position.set(75, 15);
			this.addChild(it);
		});
	}
}