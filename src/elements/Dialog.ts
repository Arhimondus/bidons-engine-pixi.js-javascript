import '../extensions';
import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import browsable from '../browsable';
import utils from '../utils';
import Container from './Container';
import Representation from '../base/Representation';
import SpriteDetector from './SpriteDetector';
import BdlfContext from '../bdlf';
import Menu from './Menu';
import { TouchBarSlider } from 'electron';
import Text from './Text';
import { Sprite } from 'pixi.js';
import Sonya from './Sonya';
import Kitana from './Kitana';
import Raiden from './Raiden';
declare var app: any;

type DialogBox = Container & {
	message: Text,
	character: Text,
}

export default class Dialog extends Container {
	static description: string = `Dialog box main container to work with BDL script files.`;

	menu: Menu;
	dialog: DialogBox;
	background: Container;
	overlay: Container;
	sprites: Container[] = [];

	get message() {
		return this.dialog.message;
	}

	get character() {
		return this.dialog.character;
	}

	constructor(public bdlfContext: BdlfContext) {
		super('Dialog', {
			size: [1280, 720],
			location: [0, 0],
			anchor: [0, 0],
		});

		this.interactive = true;
		this.defaultCursor = 'default';

		this.background = new Container('Background', {
			size: this.repr.size,
			anchor: [0, 0],
			location: [0, 0],
		}).also(it => this.addChild(it));

		this.overlay = new Container('Overlay', {
			size: this.repr.size,
			anchor: [0, 0],
			location: [0, 0],
			visible: false,
		}).also(it => this.addChild(it));

		this.sprites.push(new Sonya({
			size: [this.repr.size[0], 645],
			anchor: [0.5, 1],
			location: [1280 / 2, 720],
			visible: false,
		}, this.bdlfContext).also(it => this.addChild(it))//.also(it => bdlfContext.game.Sonya.sprite = it));
		
		this.sprites.push(new Kitana({
			size: [this.repr.size[0], 645],
			anchor: [0.5, 1],
			location: [1280 / 2, 720],
			visible: false,
		}, this.bdlfContext).also(it => this.addChild(it))//.also(it => bdlfContext.game.Kitana.sprite = it));
		
		this.sprites.push(new Raiden({
			size: [1280, 720],
			anchor: [0, 0],
			location: [250, 40],
			visible: false,
		}, this.bdlfContext).also(it => this.addChild(it))//.also(it => bdlfContext.game.Raiden.sprite = it));

		this.menu = new Menu('Menu', {
			size: [200, 200],
			anchor: [0, 0.5],
			location: [(1280 - 350) / 2, 720 / 2],
			// debug: true,
		}, this.bdlfContext).also(it => this.addChild(it));

		const character = new Text('Character', {
			size: [800, 145],
			anchor: [0, 0],
			location: [(1280 - 700) / 2 + 0, 15],
		}, {
			fontSize: 16,
			fontFamily: 'Arial',
		});
		const message = new Text('Message', {
			size: [800, 145],
			anchor: [0, 0],
			location: [(1280 - 700) / 2 + 0, 40],
		}, {
			fontSize: 16,
			fontFamily: 'Arial',
		});

		this.dialog = new Container('DialogBox', {
			size: [this.repr.size[0], 165],
			location: [1280 / 2, 720],
			anchor: [0.5, 1],
			visible: false,
		}).also(d => {
			d.character = character;
			d.message = message;
			d.addSprite(SpriteDetector(`assets/dialog_box.png`, this.repr.size[0], 165).also(s => {
				s.position.set(0, 0);
			}));
			d.addChild(d.message);
			d.addChild(d.character);
			this.addChild(d);
		});
	}

	mousedown(sprite) {
		if (this.bdlfContext.activeMenu) {
			let ovSprite = this.overlay._backgroundImage!! as any;
			if (ovSprite.hitArea.hitIndex && this.menu.menumode == 'overlay') {
				const bdlf = this.bdlfContext;
				const scene = bdlf.scene;
				const game = bdlf.game;
				const engine = bdlf.engine;
				this.bdlfContext.stopItAll = false;
				const menuItem = this.bdlfContext.activeMenu[ovSprite.hitArea.hitIndex - 1];
				this.bdlfContext.overlay();
				this.bdlfContext.activeMenu = undefined;
				const menuResult = menuItem.action(bdlf, scene, game, engine);
				console.log(`menuResult overlay /${menuItem.title} ${menuResult}`);
				if (typeof (menuResult) == 'number') {
					this.bdlfContext.run(menuResult);
				}
			} else {
				this.menu.visible = true;
				return true;
			}
		} else {
			this.bdlfContext.stopItAll = false;
			this.bdlfContext.nextRun();
		}
	}
}