import '../extensions';
import Container from './Container';
import { remote, TouchBarScrubber } from 'electron';
import Button from './Button';
import Dropdown from './Dropdown';
import Slider from './Slider';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';
import { Sprite, BaseTexture } from 'pixi.js';
import { Scrollbox } from 'pixi-scrollbox';
import * as h2d from 'hex2dec';

class SaveLoadSlot extends PIXI.Container {
	private static saveLoadTexture: PIXI.BaseTexture = PIXI.BaseTexture.from(`${rootDir}/assets/ui/saveload_slot.png`);
	private static saveLoadHoverTexture: PIXI.BaseTexture = PIXI.BaseTexture.from(`${rootDir}/assets/ui/saveload_slot__hoverState.png`);

	sprite: PIXI.Sprite;
	title: PIXI.Text;
	date: PIXI.Text;
	// preview: PIXI.Sprite;

	constructor(public id: number, date: string, preview: PIXI.Texture, public saveLoadAction: Function) { // #1 или АВТО
		super();

		this.width = 204;
		this.height = 135;
		this.interactive = true;

		this.sprite = Sprite.from(SaveLoadSlot.saveLoadTexture).also(it => {
			it.interactive = true;
			it.mouseover = () => {
				it.texture = PIXI.Texture.from(SaveLoadSlot.saveLoadHoverTexture);
			}
			it.mouseout = () => {
				it.texture = PIXI.Texture.from(SaveLoadSlot.saveLoadTexture);
			}
			it.mousedown = () => {
				this.saveLoadAction();
			}
			this.addChild(it);
		});
		this.title = new PIXI.Text(`#${id}`, { fontFamily: 'mr_StupidHeadBBG', fontSize: 14, fill: 'white', align: 'left' }).also(title => {
			title.anchor.set(0, 0.5);
			title.position.set(2, 125);
			this.addChild(title);
		});
		this.date = new PIXI.Text(date, { fontFamily: 'mr_StupidHeadBBG', fontSize: 14, fill: 'white', align: 'right' }).also(date => {
			date.anchor.set(1, 0.5);
			date.position.set(201, 125);
			this.addChild(date);
		});
		if (preview) {
			this.preview = new PIXI.Sprite(preview).also(preview => {
				preview.anchor.set(0, 0);
				preview.position.set(2, 2);
				this.addChild(preview);
			});
		}
	}
}

class NewGameSlot extends PIXI.Container {
	private static newGameTexture: PIXI.BaseTexture = PIXI.BaseTexture.from(`${rootDir}/assets/ui/newgame_slot.png`);
	private static newGameHoverTexture: PIXI.BaseTexture = PIXI.BaseTexture.from(`${rootDir}/assets/ui/newgame_slot__hoverState.png`);

	sprite: PIXI.Sprite = Sprite.from(NewGameSlot.newGameTexture);
	title: PIXI.Text;

	constructor(title: string, public saveLoadFunction: Function) {
		super();

		this.width = 204;
		this.height = 135;
		this.interactive = true;

		this.sprite = Sprite.from(NewGameSlot.newGameTexture).also(it => {
			it.interactive = true;
			it.mouseover = () => {
				it.texture = PIXI.Texture.from(NewGameSlot.newGameHoverTexture);
			}
			it.mouseout = () => {
				it.texture = PIXI.Texture.from(NewGameSlot.newGameTexture);
			}
			it.mousedown = () => {
				this.saveLoadFunction();
			}
			this.addChild(it);
		});
		this.title = new PIXI.Text(title, { fontFamily: 'mr_StupidHeadBBG', fontSize: 14, fill: 'white', align: 'center' }).also(title => {
			title.anchor.set(0.5, 0.5);
			title.position.set(100, 125);
			this.addChild(title);
		});
	}
}

export default class SaveLoad extends Container {
	static description: string = `Save-load scene.`;

	saveButton: Button;
	loadButton: Button;
	continueButton: Button;
	toMainMenuButton: Button;
	slots!: PIXI.Container[];
	scrollBox: Scrollbox;

	isSave: boolean = true;

	igt(id: string) {
		return this.bdlfContext.igt(id);
	}

	reposition() {
		let startX = 0;
		let startY = 0;
		let xGap = 25;
		let yGap = 20;
		let cols = 4;

		for (var [index, slot] of this.slots.entries()) {
			let row = Math.floor(index / cols);
			let col = index - row * cols;
			let newX = startX + col * (xGap + 204);
			let newY = startY + row * (yGap + 135);
			console.log(row, col, newX, newY);
			slot.position.set(newX, newY);
		}
	}

	toLoad() {
		console.log('It is load window!');
		if (this.slots[0] instanceof NewGameSlot) {
			let deleted = this.slots.splice(0, 1)[0];
			this.scrollBox.content.removeChild(deleted);
		}
		if (this.switchEnable) {
			this.saveButton.visible = true;
		} else {
			this.saveButton.visible = false;
		}
		this.loadButton.visible = false;
		this.reposition();
		this.isSave = false;
	}

	toSave() {
		console.log('It is save window!');
		if (this.slots[0] instanceof NewGameSlot == false) {
			this.slots.splice(0, 0, new NewGameSlot(this.igt('sl_newsave'), async () => {
				let { id, date, preview } = await this.bdlfContext.engine.saveLoader.save(this.bdlfContext);
				this.slots.splice(1, 0, new SaveLoadSlot(id, date, PIXI.Texture.from(preview), () => {
					if (this.isSave) { return; }

					let title = remote.getCurrentWindow().getTitle();
					if (remote.dialog.showMessageBox({
						buttons: ["Да", "Нет"],
						message: "Хотите перезаписать сохранение?",
						title,
					}) == 0) {
						this.bdlfContext.engine.saveLoader.save(this.bdlfContext);
						remote.dialog.showMessageBox({ message: 'Перезаписано!', title });
					}
				}).also(it => {
					this.scrollBox.content.addChild(it);
				}));
				this.reposition();
			}));
			this.scrollBox.content.addChildAt(this.slots[0], 0);
			this.reposition();
		}
		this.saveButton.visible = false;
		if (this.switchEnable) {
			this.loadButton.visible = true;
		} else {
			this.loadButton.visible = false;
		}
		this.reposition();
		this.isSave = true;
	}

	constructor(public bdlfContext: BdlfContext, isSave: boolean = true, public switchEnable = true) {
		super('Save-load', {
			size: [1280, 720],
			location: [0, 0],
			anchor: [0, 0],
		});

		PIXI.Sprite.from(`${rootDir}/assets/backgrounds/shakaled_sepia.jpg`)
			.also((it: PIXI.Sprite) => this.addChild(it));

		new PIXI.Graphics().also((g: PIXI.Graphics) => {
			g.drawRect(20, 74, 947, 628);
			g.tint = h2d.hexToDec('000000');
			g.alpha = 0.5;
			this.addChild(g);
		});

		this.scrollBox = new Scrollbox({
			boxWidth: 947 - 13 * 4,
			boxHeight: 628 - 13 * 2,
			overflowY: 'scroll',
			scrollbarOffsetVertical: 13 * 2,
		}).also(sbox => {
			sbox.position.set(20 + 13, 74 + 13);
			this.addChild(sbox);
		});

		let sqNormalState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_normalState.png`);
		let sqHoverState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_hoverState.png`);
		let sqDownState = PIXI.BaseTexture.from(`${rootDir}/assets/ui/squaredButton_downState.png`);

		let newSqNormalState = () => PIXI.Sprite.from(sqNormalState).also((sprite: PIXI.Sprite) => {
			sprite.width = 220;
			sprite.height = 47;
		});
		let newSqHoverState = () => PIXI.Sprite.from(sqHoverState).also((sprite: PIXI.Sprite) => {
			sprite.width = 220;
			sprite.height = 47;
		});
		let newSqDownState = () => PIXI.Sprite.from(sqDownState).also((sprite: PIXI.Sprite) => {
			sprite.width = 220;
			sprite.height = 47;
		});

		let sqtextOptions: PIXI.TextStyleOptions = { fontFamily: 'mr_StupidHeadBBG', fontSize: 24, fill: 'white' };

		this.loadButton = new Button('LoadButton', {
			size: [220, 47],
			anchor: [0, 0],
			location: [1020, 177],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.text = this.igt('sl_load');
			it.click = () => {
				this.toLoad();
			}
			this.addChild(it);
		});

		this.saveButton = new Button('SaveButton', {
			size: [220, 47],
			anchor: [0, 0],
			location: [1020, 263],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.text = this.igt('sl_save');
			it.click = () => {
				this.toSave();
			}
			this.addChild(it);
		});

		this.continueButton = new Button('ContinueButton', {
			size: [220, 47],
			anchor: [0, 0],
			location: [1020, 630],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.visible = this.switchEnable;
			it.text = this.igt('sl_continue');
			it.click = () => {
				this.bdlfContext.engine.application.popScene();
			}
			this.addChild(it);
		});

		this.toMainMenuButton = new Button('MainMenuButton', {
			size: [220, 47],
			anchor: [0, 0],
			location: [1020, 91],
		}, sqtextOptions, newSqNormalState(), newSqHoverState(), newSqDownState()).also(it => {
			it.text = this.igt('sl_mainmenu');
			it.click = () => {
				this.bdlfContext.hidedlg();
				this.bdlfContext.run(this.bdlfContext.dialog('main'));
				this.bdlfContext.game.Sonya.sprite.visible = false;
				this.bdlfContext.game.Kitana.sprite.visible = false;
				this.bdlfContext.game.Raiden.sprite.visible = false;
				this.bdlfContext.audios.forEach(f => { try { !f.paused ? f.pause() : null } catch (err) { } });
				this.bdlfContext.audios = [];
				this.bdlfContext.engine.game = require(rootDir + '/context.js')();
				this.bdlfContext.engine.application.popScene();
			}
			this.addChild(it);
		});

		this.bdlfContext.engine.saveLoader.getSaves().then((slots: { id: number, date: string, preview: PIXI.Texture, context: any }[]) => {
			this.scrollBox.dragScroll = slots.length > 16;
			this.slots = slots.map(save => new SaveLoadSlot(save.id, save.date, save.preview, () => {
				if (this.isSave) { return; }
				this.bdlfContext.engine.saveLoader.load(save, this.bdlfContext);
			}).also(it => this.scrollBox.content.addChild(it)));
			console.log('slots', this.slots.length);
			if (isSave) {
				this.toSave();
			} else {
				this.toLoad();
			}
		});
	}
}