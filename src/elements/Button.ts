import Container from './Container';
import Text from './Text';
import Representation from '../base/Representation';

enum ClickMode {
	Down = 'Down',
	Up = 'Up',
}

export default class Button extends Container {
	click?: Function;
	clickMode: ClickMode = ClickMode.Up;
	isMouseDown: boolean = false;
	isMouseOver: boolean = false;

	originalY?: number = undefined;
	press() {
		if (!this.isMouseDown) {
			this.originalY = this._text._text.position.y;
			this._text._text.position.y += 4;
		}
	}

	unpress() {
		this._text._text.position.y = this.originalY!!;
		this.originalY = undefined;
	}

	private _text!: Text;
	set text(value: string) {
		this._text.text = value;
	}
	get text() {
		return this._text.text as string;
	}

	constructor(
		name: string,
		repr: Representation,
		public textOptions: PIXI.TextStyleOptions,
		public normalState: PIXI.Sprite,
		public hoverState: PIXI.Sprite,
		public downState: PIXI.Sprite,
		public disableState?: PIXI.Sprite,
	) {
		super(name, repr);
		this.interactive = true;
		this.buttonMode = true;
		this.addChild(this.normalState);
		let text = new Text('ButtonText', repr, textOptions);
		this.addChild(text);
		text.let((t: Text) => {
			t._text.anchor.set(0.5, 0.5);
			t.position.set(this.repr.size[0] / 2, this.repr.size[1] / 2 - 3);
			console.log(this.repr.size[0] / 2, this.repr.size[1] / 2);
			t._text.visible = true;
			t.visible = true;
		});
		this._text = text;
	}

	mouseover() {
		this.isMouseOver = true;
		if (!this.isMouseDown) {
			this.removeChild(this.normalState);
			this.addChildAt(this.hoverState, 0);
		}
	}

	mouseout() {
		this.isMouseOver = false;
		if (!this.isMouseDown) {
			this.removeChild(this.hoverState);
			this.addChildAt(this.normalState, 0);
		}
	}

	mousedown(event: PIXI.interaction.InteractionEvent) {
		this.press();
		this.isMouseDown = true;
		this.removeChild(this.hoverState);
		this.removeChild(this.normalState);
		this.addChildAt(this.downState, 0);
		if (this.clickMode == ClickMode.Down) {
			if (this.click) {
				this.click();
			}
		}
		event.stopPropagation();
	}

	mouseup(event: PIXI.interaction.InteractionEvent) {
		this.unpress();
		this.isMouseDown = false;
		this.isMouseOver = true;
		this.removeChild(this.downState);
		this.addChildAt(this.hoverState, 0);
		if (this.clickMode == ClickMode.Up) {
			if (this.click) {
				this.click();
			}
		}
		event.stopPropagation();
	}

	mouseupoutside(event: PIXI.interaction.InteractionEvent) {
		this.unpress();
		this.isMouseDown = false;
		this.isMouseOver = false;
		this.removeChild(this.downState);
		this.addChildAt(this.normalState, 0);
		if (this.clickMode == ClickMode.Up) {
			if (this.click) {
				this.click();
			}
		}
		event.stopPropagation();
	}
}