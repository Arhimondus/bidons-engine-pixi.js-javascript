import Animation, { AnimationSprite } from './Animation';
import { Sprite } from 'pixi.js';
import Interactive from '../base/Interactive';
import rootDir from '../PathResolver';

type OverlaySprite = AnimationSprite & Interactive & {
	paths?: any[],
	hitArea: MultipleHitArea,
};

class MultipleHitArea {
	hitIndex: number | null = null;

	constructor(private hitAreas: Array<{ path: any, index: number }>) {
		hitAreas.forEach(f => {
			if (f.path.length) {
				f.path = new PIXI.Polygon(f.path.map((v: number, i: number) => i % 2 != 0 ? v - 107 : v));
			}
		});
	}

	contains(x: number, y: number) {
		// console.log(x, y);
		let item = this.hitAreas.find(f => {
			let isContains = f.path.contains(x, y);
			// console.log('contains', isContains);
			return isContains;
		});
		if (item) {
			this.hitIndex = item.index;
			// console.log(this.hitIndex);
			return true;
		} else {
			this.hitIndex = null;
			return false;
		}
	}
}

export default function Overlay(path: string, width: number, height: number, scaleMode: number = PIXI.SCALE_MODES.LINEAR): Sprite {
	let paths = require(`${rootDir.replace(/\\/g, '/')}/${path.replace('.apng', '.js')}`);
	let sprite = Animation(path, width, height, scaleMode) as OverlaySprite;

	sprite.paths = paths;
	sprite.interactive = true;
	sprite.buttonMode = true;
	sprite.hitArea = new MultipleHitArea(paths);
	sprite.stop();
	sprite.mouseover = (event) => {
		sprite.jumpToFrame(sprite.hitArea.hitIndex!);
	}
	sprite.mouseout = () => {
		sprite.jumpToFrame(0);
	}
	// sprite.mousedown = () => {
	// 	let parent = sprite.parent.parent as any;
	// 	parent.mousedown.call(parent, sprite);
	// }
	return sprite;
}