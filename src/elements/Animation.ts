import fs from 'fs';
import { Sprite, interaction } from 'pixi.js'
import PixiApngAndGif from 'pixi-apngandgif';
import rootDir from '../PathResolver';

export type AnimationSprite = Sprite & PixiApngAndGif & { __proto__: any };

export default function Animation(path: string, width: number, height: number, scaleMode: number = PIXI.SCALE_MODES.LINEAR): AnimationSprite {
	if (!global.paag_cache) global.paag_cache = {};

	let ppath = `${rootDir.replace(/\\/g, '/')}/${path.replace('.apng', '.png')}`;
	let data = fs.readFileSync(`${rootDir}/${path}`);
	if (!global.paag_cache[ppath]) {
		global.paag_cache[ppath] = new PixiApngAndGif(ppath, {
			[ppath]: {
				data,
			},
		});
	}
	// const sprite = global.paag_cache[ppath].sprite;
	const sprite = global.paag_cache[ppath].sprite as AnimationSprite;
	for(var [key, action] of Object.entries(global.paag_cache[ppath].__proto__)) {
		sprite[key] = action.bind(global.paag_cache[ppath]);
	}
	sprite.texture.baseTexture.scaleMode = scaleMode;
	sprite.position.set(0, 0);
	sprite.width = width;
	sprite.height = height;
	return sprite;
}