import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import Representation from '../base/Representation';
import Container from './Container';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';

export default class Sonya extends Container {
	static description: string = `Sonya multiclothes container.`;

	call() {
		this.visible = true;
		console.log('ctxVarSprites', this.bdlfContext.contextVars.sprites);
		if(this.bdlfContext.contextVars.sprites) {
			console.log('aaa', Object.keys(this.bdlfContext.contextVars.sprites));
		}
		if(this.bdlfContext.contextVars.sprites && Object.keys(this.bdlfContext.contextVars.sprites).length == 2) {
			this.position.set(this.position.x + 220, this.position.y);
		} else {
			this.position.set(...this.repr.location);
		}
	}
	
	away() {
		this.visible = false;
		console.log('ctxVarSprites', this.bdlfContext.contextVars.sprites);
	}
	
	_body: PIXI.Sprite;

	private get bdlfSprite() {
		if (!this.bdlfContext.contextVars.sprites) {
			this.bdlfContext.contextVars.sprites = {};
		}
		if (!this.bdlfContext.contextVars.sprites.Sonya) {
			this.bdlfContext.contextVars.sprites.Sonya = { base: 'Sonya', modifiers: {} };
		}
		if (!this.bdlfContext.contextVars.sprites.Sonya.modifiers) {
			this.bdlfContext.contextVars.sprites.Sonya.modifiers = {};
		}
		return this.bdlfContext.contextVars.sprites['Sonya'];
	}

	emotion(emotion: string) {
		this._emotion.texture = PIXI.Texture.from(`${rootDir}/assets/emotion/${emotion}.png`);
		this.bdlfContext.modify('Sonya', { ...this.bdlfSprite.modifiers, emotion });
	}
	_emotion: PIXI.Sprite;

	hairstyle(hairstyle: string) {
		this._hairstyle.texture = PIXI.Texture.from(`${rootDir}/assets/hairstyle/${hairstyle}.png`);
		this.bdlfContext.modify('Sonya', { ...this.bdlfSprite.modifiers, hairstyle });
	}
	_hairstyle: PIXI.Sprite;

	shoes(shoes: string) {
		this._shoes.texture = PIXI.Texture.from(`${rootDir}/assets/clothes/${shoes}.png`);
		this.bdlfContext.modify('Sonya', { ...this.bdlfSprite.modifiers, shoes });
	}
	_shoes: PIXI.Sprite;

	outerwear_bot(outerwear_bot: string) {
		this._outerwear_bot.texture = PIXI.Texture.from(`${rootDir}/assets/clothes/${outerwear_bot}.png`);
		this.bdlfContext.modify('Sonya', { ...this.bdlfSprite.modifiers, outerwear_bot });
	}
	_outerwear_bot: PIXI.Sprite;

	outerwear_top(outerwear_top: string) {
		this._outerwear_top.texture = PIXI.Texture.from(`${rootDir}/assets/clothes/${outerwear_top}.png`);
		this.bdlfContext.modify('Sonya', { ...this.bdlfSprite.modifiers, outerwear_top });
	}
	_outerwear_top: PIXI.Sprite;

	image_zoom(image: any, texture: any, size: [number, number]) {
		var sc = 1, ox = 0, oy = 0;

		var width = texture.width;
		var height = texture.height;

		if (height > size[1]) {
			sc = size[1] / height;
		}

		if (width > size[0]) {
			var new_sc = size[0] / width;
			if (new_sc < sc) sc = new_sc;
		}

		width = texture.width * sc;
		height = texture.height * sc;

		// Выровнять по центру по высоте
		if (height < size[1]) {
			oy = (size[1] - height) / 2;
		} else {
			oy = 0;
		}

		// Выровнять по центру по ширине
		if (width < size[0]) {
			ox = (size[0] - width) / 2;
		} else {
			ox = 0;
		}

		image.scale.set(sc, sc);
		image.position.set(ox, oy);
	}

	constructor(protected repr: Representation, public bdlfContext: BdlfContext) {
		super('Sonya', repr);

		this.visible = false;

		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);

		this._body = PIXI.Sprite.fromImage(`${rootDir}/assets/sprites/Sonya.png`);
		var _body = this._body;
		_body.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_body.texture.baseTexture.isLoading) {
			this.image_zoom(_body, _body.texture, this.repr.size);
		} else {
			_body.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_body, _body.texture, this.repr.size);
			});
		}
		this.addChild(_body);

		this._emotion = PIXI.Sprite.fromImage(`${rootDir}/assets/emotion/Default.png`);
		var _emotion = this._emotion;
		_emotion.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_emotion.texture.baseTexture.isLoading) {
			this.image_zoom(_emotion, _emotion.texture, this.repr.size);
		} else {
			_body.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_emotion, _emotion.texture, this.repr.size);
			});
		}
		this.addChild(_emotion);

		this._hairstyle = PIXI.Sprite.fromImage(`${rootDir}/assets/hairstyle/Hair_long.png`);
		var _hairstyle = this._hairstyle;
		_hairstyle.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_hairstyle.texture.baseTexture.isLoading) {
			this.image_zoom(_hairstyle, _hairstyle.texture, this.repr.size);
		} else {
			_hairstyle.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_hairstyle, _hairstyle.texture, this.repr.size);
			});
		}
		this.addChild(_hairstyle);

		// this._underwear_bot = PIXI.Sprite.fromImage(`../../game/assets/clothes/Pants.png`);
		// var _underwear_bot = this._underwear_bot;
		// _underwear_bot.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		// if(!_underwear_bot.texture.baseTexture.isLoading) {
		// this.image_zoom(_underwear_bot, _underwear_bot.texture, this.source.size);
		// } else {
		// _underwear_bot.texture.baseTexture.on('loaded', () => {
		// this.image_zoom(_underwear_bot, _underwear_bot.texture, this.source.size);
		// });
		// }
		// this.addChild(_underwear_bot);

		this._shoes = PIXI.Sprite.fromImage(`${rootDir}/assets/clothes/Shoes.png`);
		var _shoes = this._shoes;
		_shoes.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_shoes.texture.baseTexture.isLoading) {
			this.image_zoom(_shoes, _shoes.texture, this.repr.size);
		} else {
			_shoes.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_shoes, _shoes.texture, this.repr.size);
			});
		}
		this.addChild(_shoes);

		this._outerwear_bot = PIXI.Sprite.fromImage(`${rootDir}/assets/clothes/Pants.png`);
		var _outerwear_bot = this._outerwear_bot;
		_outerwear_bot.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_outerwear_bot.texture.baseTexture.isLoading) {
			this.image_zoom(_outerwear_bot, _outerwear_bot.texture, this.repr.size);
		} else {
			_outerwear_bot.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_outerwear_bot, _outerwear_bot.texture, this.repr.size);
			});
		}
		this.addChild(_outerwear_bot);

		this._outerwear_top = PIXI.Sprite.fromImage(`${rootDir}/assets/clothes/Top.png`);
		var _outerwear_top = this._outerwear_top;
		_outerwear_top.texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		if (!_outerwear_top.texture.baseTexture.isLoading) {
			this.image_zoom(_outerwear_top, _outerwear_top.texture, this.repr.size);
		} else {
			_outerwear_top.texture.baseTexture.on('loaded', () => {
				this.image_zoom(_outerwear_top, _outerwear_top.texture, this.repr.size);
			});
		}
		this.addChild(_outerwear_top);
	}
}