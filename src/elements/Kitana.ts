import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import Representation from '../base/Representation';
import Sprite from './Sprite';
import rootDir from '../PathResolver';
import BdlfContext from '../bdlf';

export default class Kitana extends Sprite {
	static description: string = `Kitana multiclothes container.`;

	call() {
		this.visible = true;
		console.log('ctxVarSprites', this.bdlfContext.contextVars.sprites);
		// if(this.bdlfContext.contextVars.sprites) {
		// 	console.log('aaa', Object.keys(this.bdlfContext.contextVars.sprites));
		// }
		if (this.bdlfContext.contextVars.sprites && Object.keys(this.bdlfContext.contextVars.sprites).length == 2) {
			this.position.set(this.position.x + 220, this.position.y);
		} else {
			this.position.set(...this.repr.location);
		}
	}

	away() {
		this.visible = false;
		console.log('ctxVarSprites', this.bdlfContext.contextVars.sprites);
	}

	hairstyle(hairstyle: string) { // "def", "n1", "n2"
		this.setSlot('hairstyle', hairstyle);
	}
	_hairstyle: PIXI.Sprite;

	bottom(bottom: string) { // "def"
		this.setSlot('bottom', bottom);
	}
	_bottom: PIXI.Sprite;

	gloves(gloves: string) { // "def"
		this.setSlot('gloves', gloves);
	}
	_gloves: PIXI.Sprite;

	pants(pants: string) { // "def"
		this.setSlot('pants', pants);
	}
	_pants: PIXI.Sprite;

	shoes(shoes: string) { // "def"
		this.setSlot('shoes', shoes);
	}
	_shoes: PIXI.Sprite;

	top(top: string) { // "def"
		this.setSlot('top', top);
	}
	_top: PIXI.Sprite;

	body(body: string) { // "front", "backview1", "backview2"
		if (body == "front") {
			this._shoes.visible = true;
			this._bottom.visible = true;
			this._gloves.visible = true;
			this._pants.visible = true;
			this._hairstyle.visible = true;
			this._top.visible = true;
		} else {
			this._shoes.visible = false;
			this._bottom.visible = false;
			this._gloves.visible = false;
			this._pants.visible = false;
			this._hairstyle.visible = false;
			this._top.visible = false;
		}
		this.setSlot('body', body);
	}
	_body: PIXI.Sprite;

	constructor(repr: Representation, bdlfContext: BdlfContext) {
		super('Kitana', repr, bdlfContext, {
			body: { path: `${rootDir}/assets/sprites/Kitana_[slotName].png`, default1: 'front' }, // this is nichantreten
			hairstyle: { path: `${rootDir}/assets/hairstyle/Kitana_[slotName]hair.png`, default1: 'def' },
			bottom: { path: `${rootDir}/assets/clothes/Kitana_[slotName]bottom.png`, default1: 'def' },
			gloves: { path: `${rootDir}/assets/clothes/Kitana_[slotName]gloves.png`, default1: 'def' },
			pants: { path: `${rootDir}/assets/clothes/Kitana_[slotName]pants.png`, default1: 'def' },
			shoes: { path: `${rootDir}/assets/clothes/Kitana_[slotName]shoes.png`, default1: 'def' },
			top: { path: `${rootDir}/assets/clothes/Kitana_[slotName]top.png`, default1: 'def' }
		});
	}
}