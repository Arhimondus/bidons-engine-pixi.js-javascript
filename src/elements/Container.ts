import * as PIXI from 'pixi.js';
import Engine from '../base/Engine';
import { remote } from 'electron';
import utils from '../utils';
import * as h2d from 'hex2dec';
import fs from 'fs';
import Representation from '../base/Representation';
import SpriteDetector from './SpriteDetector';

export default class Container extends PIXI.Container {
	static description: string = `Container with background color or image.`;

	preloadResources: string[] = [];

	bringToFront() {
		if (this.parent) {
			this.parent.setChildIndex(this, this.parent.children.length - 1);
			// var parent = this.parent; parent.removeChild(this); parent.addChild(this);
		}
	}
	
	get size(): PIXI.Point {
		return new PIXI.Point(...this.repr.size);
	}

	anchor: PIXI.ObservablePoint = new PIXI.ObservablePoint(() => {
		const pivotX = this.anchor.x * this.size.x;
		const pivotY = this.anchor.y * this.size.y;
		console.log(pivotX, pivotY);
		this.pivot.set(pivotX, pivotY);
	}, this, 0, 0);

	set backgroundColor(value: string | number) {
		if (this._backgroundColor) {
			this.removeChild(this._backgroundColor);
		}
		new PIXI.Sprite(PIXI.Texture.WHITE).let((sprite: PIXI.Sprite) => {
			sprite.width = this.repr.size[0];
			sprite.height = this.repr.size[1];
			switch (typeof (value)) {
				case 'object': // По каким-то причинам value string приходит как массив char-ов с типом object
				case 'string':
					sprite.tint = h2d.hexToDec(value.replace('#', ''));
					break;
				case 'number':
					sprite.tint = value as number;
					break;
			}
			this._backgroundColor = sprite.also((it: PIXI.Sprite) => this.addChild(it));
		});
	}
	_backgroundColor?: PIXI.Sprite;

	set backgroundImage(value: PIXI.Sprite | string) {
		if (this._backgroundImage) {
			this.removeChild(this._backgroundImage);
		}
		var sprite = typeof (value) == 'object' ? value : SpriteDetector(value, this.repr.size[0], this.repr.size[1]);
		this._backgroundImage = sprite.also((it: PIXI.Sprite) => this.addChild(it));
	}
	_backgroundImage?: PIXI.Sprite;

	constructor(public name: string, protected repr: Representation) {
		super();

		this.visible = repr.visible ?? true;
		this.position.set(...repr.location);
		this.anchor.set(...repr.anchor);

		repr.backgroundColor?.let((it: string) => this.backgroundColor = it);
		repr.backgroundImage?.let((it: PIXI.Sprite) => this.backgroundImage = it);

		this.drawBorders(repr.debug);
	}

	drawBorders(visible: boolean = false) {
		var containter = new PIXI.Container();

		containter.visible = visible;

		var graphics = new PIXI.Graphics();
		graphics.lineStyle(2, 0xFF0000);
		graphics.drawRect(0, 0, this.repr.size[0], this.repr.size[1]);
		graphics.lineStyle(2, 0x4872d5);
		graphics.drawCircle(this.repr.location[0], this.repr.location[1], 5);
		containter.addChild(graphics);

		var text = new PIXI.Text(`${this.repr.size} ${this.name}`);
		text.position.set(0, 0);
		containter.addChild(text);

		var text = new PIXI.Text(`${this.name} (${this.repr.location})`, { fontSize: 12, fill: 0x4872d5 });
		text.position.set(this.repr.location[0], this.repr.location[1]);
		containter.addChild(text);

		this.addChild(containter);
	}

	resize(size: [number, number]) {
		var width = this.repr.size[0];
		var height = this.repr.size[1];

		var sx = 1, sy = 1, ox = 0, oy = 0;

		// Вписать по высоте
		if (height > size[1]) {
			sy = size[1] / height;
			var hz = height * sy;
			var ss = width / height;
			var wz = hz * ss;
			sx = wz / width;
		}

		// Вписать до ширине
		if (width > size[0]) {
			sx = size[0] / width;
			var hz = width * sx;
			var ss = height / width;
			var wz = hz * ss;
			sy = wz / height;
		}

		// Выровнять по центру по высоте
		if (sy * height < size[1]) {
			oy = (size[1] - sy * height) / 2;
		} else {
			oy = 0;
		}

		// Выровнять по центру по ширине
		if (sx * width < size[0]) {
			ox = (size[0] - sx * width) / 2;
		} else {
			ox = 0;
		}

		this.scale.set(sx, sy);
		this.position.set(ox, oy);
	}

	addSprite(sprite: PIXI.Sprite) {
		this.addChild(sprite);
	}
}