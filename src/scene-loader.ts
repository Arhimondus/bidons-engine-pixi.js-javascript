require('./extensions.js');
import rootDir from './PathResolver';
import { Config } from './base/Config';

const BeEngine = require('./base/Engine').default;
const fs = require('fs-extra');

const conf = require(rootDir + '/bidons.json') as Config;
const game = require('./base/Game').default;
Object.assign(game, require(rootDir + '/context.js')());

const loader = (fs.existsSync(rootDir + '/loader.js') ? require(rootDir + '/loader.js') : require('./loader.js')).let(l => l.default ?? l);
const engine = new BeEngine(game, conf);

module.exports = { game, conf, loader, engine };