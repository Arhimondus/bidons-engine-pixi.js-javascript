import fs from 'fs';

function RootDir() {
	let cwd = process.cwd();
	if (fs.existsSync(`${cwd}/resources/app.asar`)) {
		cwd += '/resources/app.asar'
	}
	return cwd;
}

export default RootDir();