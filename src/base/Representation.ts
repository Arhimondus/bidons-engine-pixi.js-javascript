import { Sprite } from "pixi.js";

export default interface Representation {
	size: [number, number];
	location: [number, number];
	anchor: [number, number];
	visible?: boolean;
	backgroundColor?: string;
	backgroundImage?: Sprite;
	debug?: boolean;
}