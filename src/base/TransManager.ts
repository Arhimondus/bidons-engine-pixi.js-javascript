import Engine from './Engine';
import * as fs from 'fs';
import rootDir from '../PathResolver';

const Papa = require('papaparse');

interface CsvItem {
	id: string,
	origin: string,
	trans: string,
}

export default class TransManager {
	currentLang!: string;
	private bdlCsvs!: CsvItem[]; // Bdl Texts
	private igtCsvs!: CsvItem[]; // Ingame Texts

	constructor(public engine: Engine) {
		this.changeLanguage(this.engine.appSettings.current.language);
	}

	changeLanguage(newLang) {
		let bdlfilePath = `${rootDir}/transdata/${newLang}-bdl.csv`;
		let igtfilePath = `${rootDir}/transdata/${newLang}-igt.csv`;
		this.bdlCsvs = Papa.parse(fs.readFileSync(bdlfilePath, 'utf8'), { header: true }).data;
		this.igtCsvs = Papa.parse(fs.readFileSync(igtfilePath, 'utf8'), { header: true }).data;
		this.currentLang = newLang;
	}

	bdlCsv(id) {
		let csvItem = this.engine.transManager.bdlCsvs.find(f => f.id == id)
		if (!csvItem) {
			throw new Error(`Cannot find translation for id ${id} for language ${this.engine.transManager.currentLang}`);
		}
		return csvItem.trans;
	}

	igtCsv(id) {
		let csvItem = this.engine.transManager.igtCsvs.find(f => f.id == id)
		if (!csvItem) {
			throw new Error(`Cannot find translation for id ${id} for language ${this.engine.transManager.currentLang}`);
		}
		return csvItem.trans;
	}
}