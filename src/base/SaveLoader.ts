import * as PIXI from 'pixi.js';
import Engine from './Engine';
import mergeImg from 'merge-img';
import * as fs from 'fs-extra';
import * as Jimp from 'jimp';
import { Serializer } from './Serializer';
import Dialog from '../elements/Dialog';
import rootDir from '../PathResolver';
import SpriteUtilities from '../SpriteUtilities';
import BdlfContext from '../bdlf';

type SaveLoadItem = { id: number, date: string, preview: PIXI.Texture, context: any };

export default class SaveLoader {
	serializer: Serializer = new Serializer();

	private serializeGameContext() {
		let data = {
			Sonya: this.engine.game.Sonya.serialize(),
			Shao: this.engine.game.Shao.serialize(),
			Kitana: this.engine.game.Kitana.serialize(),
			Raiden: this.engine.game.Raiden.serialize(),
			Headvoice: this.engine.game.Headvoice,
			_bdlfc: this.engine.game._bdlfc,
			Day: this.engine.game.Day,
			value: this.engine.game.value,
		}
		console.log('serializedGameContext', JSON.stringify(data));
		return data;
	}

	withStateDir(param?) {
		return `${process.env.APPDATA}/${this.engine.config.identity}/${param ?? ''}`;
	}

	async save(bdlfContext: BdlfContext) {
		fs.ensureDirSync(this.withStateDir());

		let dateNow = new Date();
		let date = dateNow.toLocaleDateString() + ' ' + dateNow.toLocaleTimeString();

		let screenshotBuffer = this.engine.application.makeScreenShot(scene => scene instanceof Dialog);
		let jimpScreenShotBuffer = (await Jimp.read(screenshotBuffer!.buffer)).resize(200, 113);
		if (fs.existsSync(this.withStateDir(`saveloader-previews.jpg`))) {
			let asyncBuffer = await jimpScreenShotBuffer.getBufferAsync(Jimp.MIME_JPEG);
			let merged = await mergeImg([
				this.withStateDir(`saveloader-previews.jpg`),
				asyncBuffer,
			]);
			await merged.write(this.withStateDir(`saveloader-previews.jpg`));
		} else {
			await jimpScreenShotBuffer.writeAsync(this.withStateDir(`saveloader-previews.jpg`));
		}

		let saves = fs.existsSync(this.withStateDir('saves.json')) ? JSON.parse(fs.readFileSync(this.withStateDir('saves.json'), 'utf8')) : { saves: {}, nextId: 1 };
		let data = this.serializeGameContext();
		saves.saves[saves.nextId++] = data;
		saves.saves[saves.nextId - 1].date = date;
		saves.saves[saves.nextId - 1].version = this.engine.config.version;

		saves.saves[saves.nextId - 1]._bdlfc.audios = bdlfContext.packAudio();

		fs.writeFileSync(this.withStateDir('saves.json'), JSON.stringify(saves));
		let previewTexture = PIXI.BaseTexture.from(screenshotBuffer!.canvas).also((it: PIXI.BaseTexture) => {
			it.width = 200;
			it.height = 113;
		});

		try {
			PIXI.loaders.shared.resources['saveloader-previews.jpg'].texture.destroy();
		} catch (err) { }

		try {
			PIXI.loaders.shared.resources['saveloader-previews.jpg'].abort('abort');
		} catch (err) { }

		PIXI.loaders.shared.resources['saveloader-previews.jpg'] = undefined;
		PIXI.utils.TextureCache['saveloader-previews.jpg'] = undefined;

		// PIXI.loaders.shared.add('saveloader-previews.jpg', this.withStateDir(`saveloader-previews.jpg`));

		return {
			id: saves.nextId - 1,
			date,
			preview: previewTexture,
		}
	}

	load(save, bdlfContext: BdlfContext) {
		let game = this.engine.game;

		game.Sonya.deserialize(save.context.Sonya);
		game.Shao.deserialize(save.context.Shao);
		game.Raiden.deserialize(save.context.Raiden);
		game.Kitana.deserialize(save.context.Kitana);

		game.Headvoice = save.context.Headvoice;
		game.Day = save.context.Day;
		game.value = save.context.value;

		bdlfContext.restore(save.context._bdlfc);

		this.engine.application.popScene();
	}

	async quickLoad(bdlfContext) {
		let saves = await this.getSaves();
		if (saves.length == 0) { return; }
		this.load(saves[0], bdlfContext);
	}

	private getPreviews(): Promise<{ [key: string]: PIXI.Texture }> {
		return new Promise<{ [key: string]: PIXI.Texture }>((resolve, reject) => {
			let previewTexture = PIXI.loaders.shared.resources['saveloader-previews.jpg'];
			let u = new SpriteUtilities(PIXI);
			if (previewTexture) {
				try {
					let textures = u.filmstrip(`saveloader-previews.jpg`, 200, 113);
					resolve(textures);
				} catch (err) {
					resolve({});
				}
			} else {
				if (!fs.existsSync(this.withStateDir(`saveloader-previews.jpg`))) {
					return resolve({});
				}
				PIXI.loaders.shared.add('saveloader-previews.jpg', this.withStateDir(`saveloader-previews.jpg`));
				PIXI.loaders.shared.load((loader, resources) => {
					previewTexture = resources['saveloader-previews.jpg'];
					try {
						let textures = u.filmstrip(`saveloader-previews.jpg`, 200, 113);
						resolve(textures);
					} catch (err) {
						resolve({});
					}
				});
			}
		});
	}

	async getSaves(): Promise<SaveLoadItem[]> {
		let previewsTextures = await this.getPreviews();
		try {
			let saves: { saves: { [key: string]: any } } = JSON.parse(fs.readFileSync(this.withStateDir('saves.json'), 'utf8'));
			let arr: SaveLoadItem[] = [];
			for (var index of Object.keys(saves.saves).filter(f => f != 'also' && f != 'let')) {
				let item = {
					id: +index,
					preview: previewsTextures[+index - 1],
					date: saves.saves[index].date,
					context: saves.saves[index],
				} as SaveLoadItem;
				arr.push(item);
			}
			return arr.sort((a, b) => b.id - a.id);
		} catch (err) {
			return [];
		}
	}

	constructor(public engine: Engine) {
		// mergeImg(['image-1.png', 'image-2.jpg'])
		// 	.then((img) => {
		// 		// Save image as file
		// 		img.write('out.png', () => console.log('done'));
		// 	});
	}
}