import * as PIXI from 'pixi.js';
import Container from '../elements/Container';
import { TouchBarSlider } from 'electron';
import Dialog from '../elements/Dialog';
import Settings from '../elements/Settings';
import SaveLoad from '../elements/SaveLoad';
import Frame from 'canvas-to-buffer';

type SceneType = (typeof PIXI.Container);

export default class PixiApplication extends PIXI.Application {
	availableScenes: { [key: string]: any } = {
		'Dialog': Dialog,
		'Settings': Settings,
		'SaveLoad': SaveLoad,
	};
	allScenes: PIXI.Container[] = [];

	readonly scene: PIXI.Container = new PIXI.Container();
	readonly overlay: PIXI.Container = new PIXI.Container();
	readonly width: number;
	readonly height: number;

	/// Make screenshot of current scene
	makeScreenShot(predicate): { buffer: Buffer, canvas: HTMLCanvasElement } | undefined {
		let scene = this.allScenes.find(predicate);
		if (scene) {
			let canvas = this.renderer.plugins.extract.canvas(scene) as HTMLCanvasElement;
			let frame = new Frame(canvas, { image: { types: ['jpeg'] } });
			console.log(frame.getImageType());
			return { buffer: frame.toBuffer(), canvas };
		}
		return undefined;
	}

	mergeScenes() {

	}

	currentScene() {
		return this.scene.getChildAt(0);
	}

	replaceScene(scene: Container) {
		this.scene.removeChildren();

		this.allScenes.pop()?.destroy();

		this.preloadResources(scene.preloadResources);

		this.scene.addChild(scene);
		this.allScenes.push(scene);
	}

	pushScene(scene: Container) {
		this.scene.removeChildren();

		this.preloadResources(scene.preloadResources);

		this.scene.addChild(scene);
		this.allScenes.push(scene);

		console.log('Pushed 1 scene!');
		console.log(`Now are ${this.allScenes.length}`);
	}

	popScene() {
		this.scene.removeChildren();

		this.allScenes.pop()?.destroy();
		let lastScene = this.allScenes[this.allScenes.length - 1];
		this.scene.addChild(lastScene);
	}

	setOverlay(overlay: Container) {
		this.overlay.removeChildren(0).forEach(child => child.destroy());

		this.preloadResources(overlay.preloadResources);
		this.overlay.addChild(overlay);
	}

	constructor(args: PIXI.ApplicationOptions) {
		super(args);
		this.width = args.width ?? 250;
		this.height = args.height ?? 250;
		this.stage.addChild(this.scene);
		this.stage.addChild(this.overlay);
		this.view.id = 'main';
		window.addEventListener('resize', () => this.resize());
	}

	private resize() {
		if (this.scene) {
			let parent = this.view.parentNode as any;
			// this.renderer.resize(parent.clientWidth, parent.clientHeight);
			// debugger;
			// this.renderer.resize(this.width, this.height);
			// this.stage.width = this.width;
			// this.stage.height = this.height;
			document.querySelector('canvas').style.width = parent.clientWidth + 'px';
			document.querySelector('canvas').style.height = parent.clientWidth * 9 / 16 + 'px';
		}
	}

	private preloadResources(preloadResources: string[]) {
		// TODO: ...
	}

	ready() {
		document.getElementById('canvas')!!.append(this.view);
		this.resize();
	}
}