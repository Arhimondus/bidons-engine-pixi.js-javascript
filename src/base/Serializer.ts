export interface ISerializable {
	serialize(): any;
	getClass?(): any;
	deserialize(data: any);
}

export class Serializer {
	static classes: any = [];
	static registerClass(type) {
		Serializer.classes.push(type);
	}
}