import BeSaveLoader from './SaveLoader';
import PixiApplication from './PixiApplication';
import TransManager from './TransManager';
import { remote } from 'electron';
import { Config } from './Config';
import * as fs from 'fs-extra';
import AppSettings from './AppSettings';
import Singleton from '../Singleton';

export default class Engine extends Singleton {
	appSettings: AppSettings = new AppSettings(this);
	saveLoader: BeSaveLoader = new BeSaveLoader(this);
	transManager: TransManager = new TransManager(this);

	application: PixiApplication = new PixiApplication({
		width: 1280,
		height: 720,
		// view: document.getElementById('main') as HTMLCanvasElement,
		roundPixels: true,
		transparent: true
	});

	constructor(public game: any, public config: Config) {
		super();
		remote.getCurrentWindow().setTitle(config.window.title);
	}
}