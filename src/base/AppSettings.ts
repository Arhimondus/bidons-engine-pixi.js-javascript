import Engine from "./Engine";
import * as fs from 'fs-extra';

export default class AppSettings {
	current: any;

	constructor(public engine: Engine) {
		this.loadSettings();
	}

	withStateDir(param?) {
		return `${process.env.APPDATA}/${this.engine.config.identity}/${param ?? ''}`;
	}

	loadSettings() {
		let defaultSettings = {
			language: Intl.DateTimeFormat().resolvedOptions().locale.slice(0, 2), // this.engine.config.defaultLanguage,
			screenMode: 'window',
			skipSpeed: 'medium',
			coloredCharacters: true,
		};
		if (fs.existsSync(this.withStateDir('settings.json'))) {
			try {
				this.current = JSON.parse(fs.readFileSync(this.withStateDir('settings.json'), 'utf8'));
			} catch (err) {
				this.current = defaultSettings;
			}
		} else {
			this.current = defaultSettings;
		}
		console.log('settings loaded!');
	}

	saveSettings() {
		fs.writeFileSync(this.withStateDir('settings.json'), JSON.stringify(this.current));
		console.log('settings changed!');
	}
}