export interface Config {
	identity: string;
	version: string,
	defaultFont?: string,
	startingScript?: string,
	startingScene?: string,
	languages?: string[],
	bdlLanguage?: string,
	defaultLanguage?: string,
	window: { title: string, width: number, height: number },
}