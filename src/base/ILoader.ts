import Engine from './Engine';

type ILoader = (engine: Engine, outerContext: any) => void;
export default ILoader;
