interface Function {
	bind<F extends Function>(this: F, thisArg: any): F
}

export default interface Interactive {
	click?(event: PIXI.interaction.InteractionEvent): void;
	mousedown?(event: PIXI.interaction.InteractionEvent): void;
	mousemove?(event: PIXI.interaction.InteractionEvent): void;
	mouseout?(event: PIXI.interaction.InteractionEvent): void;
	mouseover?(event: PIXI.interaction.InteractionEvent): void;
	mouseup?(event: PIXI.interaction.InteractionEvent): void;
	mouseupoutside?(event: PIXI.interaction.InteractionEvent): void;
}