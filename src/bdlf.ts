import * as fs from 'fs';
import Script, { MenuItem } from './Script';
import Engine from './base/Engine';
import Dialog from './elements/Dialog';
import rootDir from './PathResolver';
import { remote, TouchBarScrubber } from 'electron';
import Singleton from './Singleton';

interface Audio {
	id: string;
	file: string;
	loop: boolean;
	currentTime: number;
	paused: boolean;
	pause(): any;
	play(): any;
}

interface PackedAudio {
	id: string;
	file: string;
	loop: boolean;
}

interface BdlfContextVars {
	sprites: { [key: string]: { base: string, modifiers: any } };
	background?: string;
	overlay?: string;
	character?: string;
	replica?: string;
	menumode: string;

	dialog: string;
	position: number;
	audios: { nonActive: PackedAudio[], active: PackedAudio[] };
}

export default class BdlfContext extends Singleton {
	eesama: PIXI.utils.EventEmitter = new PIXI.utils.EventEmitter();

	bdlfContextGameKey: string = '_bdlfc';

	get contextVars(): BdlfContextVars {
		if (!this.engine.game[this.bdlfContextGameKey]) {
			this.engine.game[this.bdlfContextGameKey] = {};
		}
		return this.engine.game[this.bdlfContextGameKey];
	}

	get game() {
		return this.engine.game;
	}

	script?: Script;
	name?: string;
	activeMenu?: MenuItem[];
	audios: Audio[] = [];

	stopItAll: boolean = false;
	afterJmStop: boolean = false;

	private position: number = -1;
	private lines?: string[];

	packAudio(): { nonActive: PackedAudio[], active: PackedAudio[] } {
		let nonActive = this.audios.filter(f => f.paused == true).map(m => ({ id: m.id, file: m.file, loop: m.loop }));
		let active = this.audios.filter(f => f.paused == false).map(m => ({ id: m.id, file: m.file, loop: m.loop }));
		return { nonActive, active };
	}

	unpackAudio(audios: { nonActive: PackedAudio[], active: PackedAudio[] }) {
		this.audios.forEach(f => { try { !f.paused ? f.pause() : null } catch (err) { } });
		this.audios = [];

		audios.nonActive.forEach(nonA => {
			let audio = new Audio(`${rootDir}/assets/` + nonA.file) as unknown as Audio;
			audio.id = nonA.id;
			audio.file = nonA.file;
			audio.loop = nonA.loop;
			this.audios.push(audio);
		});

		audios.active.forEach(nonA => {
			let audio = new Audio(`${rootDir}/assets/` + nonA.file) as unknown as Audio;
			audio.id = nonA.id;
			audio.file = nonA.file;
			audio.loop = nonA.loop;
			this.audios.push(audio);
			audio.play();
		});
	}

	byId(id: string) {
		return this.engine.transManager.bdlCsv(id);
	}

	igt(id: string) {
		return this.engine.transManager.igtCsv(id);
	}

	private _krücke?: Dialog;
	get scene() {
		if (!this._krücke) {
			this._krücke = this.engine.application.currentScene() as Dialog;
		}
		return this._krücke;
	}

	constructor(public engine: Engine) {
		super();
	}

	get context() {
		return {
			bdlf: this,
			scene: this.scene,
			game: this.game,
			engine: this.engine
		};
	}

	get contextArr() {
		return [
			this,
			this.scene,
			this.game,
			this.engine,
		];
	}

	lang() {
		return this.engine.appSettings.current.language;
	}

	resetContext() {
		let contextJs = require(rootDir + '/context.js');
		this.engine.game = contextJs();
		this.overlay();
		this.background();
		// this.game.Sonya.sprite = this.engine.application
		// 	.allScenes.find(f => f.constructor.name == 'Dialog')?.children.find(f => f.constructor.name == 'Sonya');
		console.log('+++++++++++++++++++++++ RESETED! +++++++++++++++++++++++ ');
	}

	// Restore state from contextVars
	restore(_bdlfc: BdlfContextVars) {
		this.audios.forEach(f => f.pause());
		this.audios = [];

		this.menumode(_bdlfc.menumode);

		this.background(_bdlfc.background);
		if (_bdlfc.character) {
			this.character(_bdlfc.character);
		}
		if (_bdlfc.replica) {
			this.replica(_bdlfc.replica);
		}

		this.overlay();
		this.activeMenu = undefined;

		if (_bdlfc.sprites && _bdlfc.sprites.Sonya) {
			// this.game.Sonya.sprite = this.engine.application.allScenes.find(f => f.constructor.name == 'Dialog')?.children.find(f => f.constructor.name == 'Sonya');
			console.log('Sonya modifiers', _bdlfc.sprites.Sonya.modifiers);
			this.game.Sonya(_bdlfc.sprites.Sonya.modifiers);
			this.game.Sonya.call();
		} else {
			this.game.Sonya.sprite.visible = false;
		}

		if (_bdlfc.sprites && _bdlfc.sprites.Kitana) {
			// this.game.Kitana.sprite = this.engine.application.allScenes.find(f => f.constructor.name == 'Dialog')?.children.find(f => f.constructor.name == 'Kitana');
			this.game.Kitana.sprite.visible = true;
			console.log('Kitana modifiers', _bdlfc.sprites.Kitana.modifiers);
			this.game.Kitana(_bdlfc.sprites.Kitana.modifiers);
			this.game.Kitana.call();
		} else {
			this.game.Kitana.sprite.visible = false;
		}

		if (_bdlfc.sprites && _bdlfc.sprites.Raiden) {
			// this.game.Raiden.sprite = this.engine.application.allScenes.find(f => f.constructor.name == 'Dialog')?.children.find(f => f.constructor.name == 'Raiden');
			this.game.Raiden(_bdlfc.sprites.Raiden.modifiers);
			console.log('Raiden modifiers', _bdlfc.sprites.Raiden.modifiers);
			this.game.Raiden(_bdlfc.sprites.Raiden.modifiers);
			this.game.Raiden.call();
		} else {
			this.game.Raiden.sprite.visible = false;
		}

		if (_bdlfc.overlay) {
			this.overlay(_bdlfc.overlay);
		}

		this.unpackAudio(_bdlfc.audios);

		this.dialog(_bdlfc.dialog);
		this.run(_bdlfc.position);
	}

	sprite(spriteId, sprite) {
		console.log('sprite', spriteId, sprite);
		// sprite 'Sonya', 'Sonya'
		if (!this.contextVars.sprites) {
			this.contextVars.sprites = {};
		}
		this.contextVars.sprites[spriteId] = { base: sprite, modifiers: null };
		this.eesama.emit('sprite', { spriteId, sprite });
	}

	removeSprite(spriteId) {
		console.log('removeSprite', spriteId);
		delete this.contextVars.sprites;
	}

	modify(spriteId, modify) {
		if (!this.contextVars.sprites) {
			this.contextVars.sprites = {};
		}
		// modify emotion: 'e', bottom: 'b', top: 't', 'Sonya'
		this.contextVars.sprites[spriteId].modifiers = modify;
		this.eesama.emit('modify', { modify, spriteId });
	}

	saveGame() {
		this.engine.saveLoader.save(this);
	}

	loadGame() {
		this.engine.saveLoader.quickLoad(this);
	}

	replaceScene(sceneName: string, arg?: any, arg2?, arg3?) {
		let scene = new this.engine.application.availableScenes[sceneName](this, arg, arg2, arg3);
		this.engine.application.replaceScene(scene);
	}

	pushScene(sceneName: string, arg?: any, arg2?, arg3?) {
		let scene = new this.engine.application.availableScenes[sceneName](this, arg, arg2, arg3);
		this.engine.application.pushScene(scene);
	}

	run(position: number) {
		if (this.stopItAll) {
			console.log(`Nein, nein!`);
			return;
		}

		this.position = position;
		this.contextVars.position = position;
		const flowItem = this.script?.flow[this.position];
		if (!flowItem) {
			try {
				throw new Error(`Position ${this.position} out of range!`);
			} catch (err) { return console.error(err); }
		}
		this.eesama.emit('line', { position: this.position, line: flowItem.line });
		console.log(`# run ${this.position} (${flowItem.line})`);
		let flowResult = flowItem.action(...this.contextArr);

		if (typeof (flowResult) == 'number') {
			this.run(flowResult);
		} else if (!flowResult) {
			this.nextRun();
		}
	}

	jump(position: number) {
		console.log(`jump`);
		// this.position = position;
		return position;
	}
	goto(label: string) {
		console.log(`goto`);
		// this.position = this.script?.labels[label] as number;
		return this.script?.labels[label] as number;
	}
	next() {
		console.log(`next`);
		// this.position += 1;
		return this.position + 1;
	}

	fastGo(label) {
		this.run(this.goto(label));
	}

	showdlg() {
		console.log(`showdlg`);
		this.scene.dialog.visible = true;

		return false;
	}
	hidedlg() {
		console.log(`hidedlg`);
		this.scene.dialog.visible = false;

		return false;
	}
	replica(message: string) {
		console.log(`replica ${message}`);
		this.contextVars.replica = message;

		if (!this.scene.dialog.visible) {
			this.scene.dialog.visible = true;
		}
		this.scene.dialog.message.text = message;

		return true;
	}
	character(character: string) {
		console.log(`character ${character}`);
		this.contextVars.character = character;

		this.scene.character.text = this.igt(character);

		if (this.engine.appSettings.current.coloredCharacters) {
			switch (character) {
				case 'Шао-Кан':
					this.scene.character.color = '#e07676';
					break;
				case 'Соня':
					this.scene.character.color = '#00914f';
					break;
				case 'Китана':
					this.scene.character.color = '#4a5ed3';
					break;
				case 'Рейден':
					this.scene.character.color = '#5f90be';
					break;
				default:
					this.scene.character.color = '#ffffff';
					break;
			}
		} else {
			this.scene.character.color = '#ffffff';
		}

		return false;
	}
	background(background?: string) {
		if (!background) {
			this.scene.background.visible = false;
			this.contextVars.background = undefined;
			this.eesama.emit('background', null);
			return false;
		}

		console.log(`background ${background}`);
		this.scene.background.backgroundImage = `assets/${background}`;
		this.scene.background.visible = true;

		this.contextVars.background = background;
		this.eesama.emit('background', background);

		return false;
	}
	overlay(overlay?: string) {
		if (!overlay) {
			this.scene.overlay.visible = false;
			this.contextVars.overlay = undefined;
			this.eesama.emit('overlay', null);
			return false;
		}

		console.log(`overlay ${overlay}`);
		this.scene.overlay.backgroundImage = `assets/${overlay}`;
		this.scene.overlay.visible = true;

		this.contextVars.overlay = overlay;

		this.contextVars.replica = undefined;
		this.contextVars.character = undefined;

		this.eesama.emit('overlay', overlay);

		return false;
	}
	dialog(dialog: string) {
		let hmsg = `dialog ${dialog}`;
		let hr = new Array(hmsg.length + 1).join('-');
		console.log(' ');
		console.log(hmsg);
		console.log(hr);

		// this.position = 0;
		this.lines = fs.readFileSync(`${rootDir}/scripts/${dialog}.bdl`, 'utf8').split('\r\n').map(m => m.trim());
		this.script = require(`${rootDir}/.generated/love/scripts/${dialog}.js`);
		this.name = dialog;

		this.contextVars.dialog = dialog;
		this.eesama.emit('dialog', { name: this.name, lines: this.lines });

		return 0;
	}
	random(min: number, max: number) {
		console.log(`random ${min} ${max}`);
		var min = Math.ceil(min);
		var max = Math.floor(max);

		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	label(label: string) {
		console.log(`label ${label}`);

		return false;
	}

	menu(menuIndex: number) {
		console.log(`menu ${menuIndex}`);
		this.activeMenu = this.script?.menus[menuIndex];
		this.scene.menu.changeMenu(this.activeMenu, this.context);

		return true;
	}

	exit(menuIndex: number, action: Function | number) {
		console.log(`exit ${menuIndex}`);
		this.activeMenu = undefined;
		if (typeof (action) == 'function') {
			return action(...this.contextArr);
		} else {
			return action;
		}
	}

	menumode(menumode: string) {
		this.scene.menu.menumode = menumode;
		this.contextVars.menumode = menumode;

		if (menumode == 'default') {
			this.overlay();
		}
		return false;
	}

	play(name: string, id: string, cyclic = false) {
		console.log(`play ${name} ${id}`);
		if (!id) {
			id = `a${(~~(Math.random() * 1e8)).toString(16)}`;
		}

		let prevAudio = id ? this.audios.find((f: Audio) => f.id == id) : this.audios[this.audios.length - 1];

		if (prevAudio) {
			prevAudio.currentTime = 0;
			prevAudio.play();
			return;
		}

		let audio = new Audio(`${rootDir}/assets/` + name) as unknown as Audio;
		audio.id = id;
		audio.file = name;
		audio.loop = cyclic;
		audio.play();
		this.audios.push(audio);

		return false;
	}
	stop(id: string) {
		console.log(`stop ${id}`);
		let audio = id ? this.audios.find((f: Audio) => f.id == id)! : this.audios[this.audios.length - 1];
		if (!audio) return;
		audio.pause();
		this.audios = this.audios.filter((f: Audio) => f.id != audio.id);

		return false;
	}
	pause(id: string) {
		console.log(`pause ${id}`);
		let audio = id ? this.audios.find((f: Audio) => f.id == id) : this.audios[this.audios.length - 1];
		if (!audio) return;
		audio.pause();

		return false;
	}
	continue(id: string) {
		console.log(`continue ${id}`);
		let audio = id ? this.audios.find((f: Audio) => f.id == id) : this.audios[this.audios.length - 1];
		if (!audio) return;
		audio.play();

		return false;
	}
	quit() {
		remote.getCurrentWindow().close();
	}

	nextRun() {
		this.run(this.next());
	}
}