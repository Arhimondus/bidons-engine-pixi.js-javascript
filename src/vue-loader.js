const Vue = require('vue/dist/vue.js');
const Mousetrap = require('mousetrap');
const glob = require('glob');
const { parse, stringify } = require('flatted/cjs');
const CircularJSON = require('circular-json');

module.exports = function (engineContext, loader) {
	Vue.directive('click-outside', {
		bind: function (el, binding, vnode) {
			el.clickOutsideEvent = function (event) {
				// here I check that click was outside the el and his childrens
				if (!(el == event.target || el.contains(event.target))) {
					// and if it did, call method provided in attribute value
					vnode.context[binding.expression](event);
				}
			};
			document.body.addEventListener('click', el.clickOutsideEvent)
		},
		unbind: function (el) {
			document.body.removeEventListener('click', el.clickOutsideEvent)
		},
	});

	Vue.component('tree_node', {
		props: {
			object: Object,
			hidesub: { default: true },
		},
		computed: {
			has_sub_items() {
				return this.object.source.controls && this.object.source.controls.length > 0;
			},
			selected() {
				return this.$root.selected_object == this.object;
			},
			fixed() {
				return this.$root.fixed_object == this.object;
			},
		},
		methods: {
			select() {
				// this.$emit('select_object', this.object);
				this.$root.select_object(this.object);
				console.log(this.object.source.name);
			},
		},
		template: `<li :class="{ '--fixed': fixed }">
					<template v-if="object.source">
						<div class="object" :class="{ '--selected': selected }">
							<span class="object__line"></span>
							<span class="object__expand" @click="hidesub = !hidesub" v-bind:style="{ visibility: has_sub_items ? 'visible' : 'hidden' }"><template v-if="hidesub">+</template><template v-else>-</template></span>
							<span class="object__icon" :style="{ 'background-image': 'url(' + object.type_icon + ')' }"></span>
							<span class="object__locked"></span>
							<span class="object__status"></span> <!-- Выбран / не выбран, внутри / не внутри -->
							<span class="object__name" style="cursor: pointer;" @click="select">{{ object.source.name }}</span>
						</div>
					</template>
					<ul class="controls" v-bind:class="{ hidden: hidesub }">
						<tree_node v-for="child in object.children" :object="child"/>
					</ul>
				</li>`,
	});
	function isScrolledIntoView(el) {
		var rect = el.getBoundingClientRect();
		var elemTop = rect.top;
		var elemBottom = rect.bottom;

		// Only completely visible elements return true:
		var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
		// Partially visible elements return true:
		//isVisible = elemTop < window.innerHeight && elemBottom >= 0;
		return isVisible;
	}

	function clone(obj) {
		var copy = {};
		for (var key of obj.params) {
			copy[key] = obj[key];
		}
		return copy;
	}

	global.app = new Vue({
		el: '#vue-container',
		data: {
			engineContext,
			lines: [],
			index: 0,
			script: 'undefinded',
		},
		filters: {
			description(objtype) {
				return objtype.prototype.constructor.description;
			},
		},
		methods: {
			getGameContext() {
				be.game['@Sonya'] = clone(be.game.Sonya);
				this.gamecontext = CircularJSON.stringify(be.game, null, '  ');
			},
			stopSkip() {
				clearInterval(this.dialog.skipHandler);
				this.dialog.skipHandler = null;
			},
			skip() {
				if (!this.dialog.skipHandler) {
					this.dialog.skipHandler = setInterval(() => {
						if (!this.dialog.menu) {
							this.next('skip');
						} else {
							// this.stopSkip(); // Раскомментировать для остановки скипа после выбора меню
						}
					}, 50);
				} else {
					this.stopSkip();
				}
			},
			highlightLine(line) {
				document.getElementById(`LINE${line}`).let(elem => {
					try {
						if (!isScrolledIntoView(elem)) {
							elem.scrollIntoView(false);
						}
					} catch (err) { }
				});
			},
			container() {
				if (this.selected_object && this.selected_object.container) {
					return this.selected_object;
				} else {
					return this.be.pixi.scene;
				}
			},
			icon(objtype) {
				return objtype.prototype.type_icon;
			},
			add_new_object() {
				if (this.selected_object_type) {
					var add_to = this.container();
					var new_object = new this.selected_object_type(this.be);
					add_to.addChild(new_object);
					add_to.source.controls.push(new_object.source);
					this.phase = 'scene';
				}
			},
			hide_settings() {
				if (this.display_settings && this.display_settings_clicks == 2)
					this.display_settings = false;
				else
					this.display_settings_clicks++;
			},
			show_settings() {
				// debugger;
				this.display_settings = !this.display_settings;
				this.display_settings_clicks = 1;
				this.$forceUpdate();
			},
			save_scene() {
				if (this.scene_path == null) {
					// Показать диалог ввода названия файла

				} else {
					this.be.konstabel.keep();
					global.alert('Успешно!');
				}
			},
			add_new_moulage() {
			},
			apply_select_asset(path) {
				this.selected_object.image = path;
				this.phase = 'scene';
			},
		},
		computed: {

		},
		mounted() {
			loader(be, this);
		},
	});

}