Object.prototype.also = function(callback) {
	if (typeof(callback) == 'function') {
		callback(this);
	}
	return this;
};

Object.prototype.let = function(callback) {
	if (typeof(callback) == 'function') {
		return callback(this);
	}
	return null;
};