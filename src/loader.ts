import Engine from './base/Engine';
import BdlfContext from './bdlf';
import Dialog from './elements/Dialog';
import Settings from './elements/Settings';
import rootDir from './PathResolver';

export default function loader(engine: Engine, conf: any, outerContext: any) {
	const bdlfContext = new BdlfContext(engine);

	function start() {
		let loader = new PIXI.loaders.Loader();
		let stupidFontPath = `${rootDir}/assets/fonts/mrStupidhead.ttf`;
		loader.add('stupidhead', stupidFontPath);
		loader.load((abc, vgd) => {
			// const dialog = new Settings(bdlfContext);
			// const dialog = new Dialog(bdlfContext);
			// engine.application.pushScene(dialog);
			bdlfContext.replaceScene('Dialog');
			bdlfContext.run(bdlfContext.dialog('main'));
		});
	}

	return { bdlfContext, start };
}