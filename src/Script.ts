export interface MenuItem {
	title: String;
	action: Function;
}

// export interface Menu {
// 	type: string;
// 	menues: Item[];
// }

export interface Flow {
	line: number;
	delayed: boolean;
	action: Function;
}

export default interface Script {
	labels: { [key: string]: number },
	// menus: Menu[],
	menus: MenuItem[][],
	flow: Flow[],
}