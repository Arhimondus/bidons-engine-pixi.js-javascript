import * as PIXI from 'pixi.js';
import fs from 'fs';
import * as Handlebars from 'handlebars';
import Dialog from './elements/Dialog';
import BdlfContext from './bdlf';

export default {
	parse_bn(project_path: string, file: string, classes: any, extra_arg: any): any {
		return require('./parsers/bn-parser.js')(require('fs').readFileSync(project_path + '/' + file, 'utf-8'), classes, extra_arg);
	},
	parse_bdl(project_path: string, file: string, characters: object[]) {
		return require('./parsers/bdl-parser.js')(require('fs').readFileSync(project_path + '/' + file, 'utf-8'), characters);
	},
	parse_json(project_path: string, file: string, classes: any, extra_arg: any): any {
		var scene = require(project_path + '/' + file.replace('.bscn', '.json'));
		var node = new classes[scene.root_node._class](extra_arg.be, scene.root_node)
		node.width = scene.options.size[0];
		node.height = scene.options.size[1];
		return { node, options: scene.options, load: scene.load };
	},
	empty_scene(classes: any, extra_arg: any): any {
		var node = new classes['container'](extra_arg.be)
		node.width = 1280;
		node.height = 720;
		return node;
	},
	simple_text_property(title: string, object_key: string, source_object_key = object_key, parent: any) {
		return {
			title: title,
			type: 'Text',
			set value(value: string) {
				parent[object_key] = value;
				parent.source[object_key] = value;
			},
			get value(): string {
				return parent.source[source_object_key];
			}
		}
	},
	simple_tuple_property(title: string, object_key: string, source_object_key = object_key, parent: any, ext_set?: Function) {
		return {
			title: title,
			type: 'Tuple',
			set value(value: any) {
				value = value.split(',').map((m: string) => +m);
				parent[object_key].set(...value);
				parent.source[source_object_key] = value;
				if (ext_set)
					ext_set();
			},
			get value(): any {
				return parent.source[source_object_key].join(', ');
			}
		}
	},
	simple_boolean_property(title: string, object_key: string, source_object_key = object_key, parent: any, ext_set?: Function) {
		return {
			title: title,
			type: 'Boolean',
			set value(value: any) {
				// value = value.split(',').map((m: string) => +m);
				parent[object_key] = value;
				parent.source[source_object_key] = value;
				if (ext_set)
					ext_set();
			},
			get value(): any {
				return parent.source[source_object_key];
			}
		}
	},
	properties(parent: any, func: any) {
		return func(parent);
	},
	dev_border(element: any) {
		// var dev_border = new PIXI.Sprite(PIXI.Texture.WHITE);
		// dev_border.width = source.size[0];
		// dev_border.height = source.size[1];
		// dev_border.tint = 0x7f0000;
		// dev_border.visible = false;

		// var inner = new PIXI.Sprite(PIXI.Texture.WHITE);
		// inner.x = 1;
		// inner.y = 1;
		// inner.width = dev_border.width - 2;
		// inner.height = dev_border.height -2;
		// inner.tint = 0x7f000000;
		// inner.visible = true;

		// dev_border.addChild(inner);

		// var dev_border = new PIXI.Container();

		var graphics: any = new PIXI.Graphics();
		graphics.update = () => {
			graphics.clear();
			// graphics.beginFill(0xFFFFFF, 0);
			graphics.lineStyle(2, 0xFF0000);
			graphics.drawRect(-element.size.x * element.anchor.x, -element.size.y * element.anchor.y, element.size.x, element.size.y);
			// console.log(-element.size.x * element.anchor.x, -element.size.y * element.anchor.y, element.size.x, element.size.y);
		}
		graphics.visible = false;
		graphics.update();

		element.dev_border = graphics;
		element.addChild(element.dev_border);

		return graphics;
	},
	dev_origin(element: any) {
		var graphics: any = new PIXI.Graphics();
		graphics.update = () => {
			graphics.clear();
			graphics.beginFill(0x6833FF, 1);
			graphics.lineStyle(2, 0x688EFF);
			graphics.drawCircle(element.size.x * element.anchor.x, element.size.y * element.anchor.y, 3);
		}
		graphics.visible = false;
		graphics.update();

		element.dev_origin = graphics;
		element.addChild(element.dev_origin);

		return graphics;
	},
	dev_settings_change(element: any, func: Function) {
		if (element.owner.editor_mode)
			element.owner.editor.eesama.on('settings_changes', func, element);
	},
	read_json_file(path: string, base_path?: string) {
		if (base_path)
			return JSON.parse(fs.readFileSync(base_path + '/' + path, 'utf8'));
		else
			return JSON.parse(fs.readFileSync(path, 'utf8'));
	},
	get_settings() {
		return this.read_json_file('.waesettings', __dirname);
	},
	read_template(template_name: string) {
		var template_content = fs.readFileSync(`templates/${template_name}`);
		return Handlebars.compile(template_content);
	},
	dialog_scene(classes: any, extra_arg: any): any {
		const engine = extra_arg.be;
		return new Dialog(new BdlfContext(engine));
	},

}