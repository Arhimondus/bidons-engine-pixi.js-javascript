import 'reflect-metadata';

export default function browsable(title: string, converter: Function) {
	return function decorator(target: any, property: string, descriptor: PropertyDescriptor) {
		Reflect.defineMetadata('title', title, target, property);
		Reflect.defineMetadata('converter', converter, target, property);
	}
}