import { app, BrowserWindow, ipcMain } from 'electron';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
	// app.quit();
// }

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow: BrowserWindow | null;
// let beApp: BeProject;

const createWindow = () => {
	ipcMain.on('setBeApplication', (event: any /*, be_app: BeProject*/) => {
		// beApp = be_app;
	});
	
	// Create the browser window.
	mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		show: true,
		title: 'Bidons Engine',
		webPreferences: { nodeIntegration: true },
	});
	
	mainWindow.setMenu(null);

	/*let splash = new BrowserWindow({ width: 856, height: 571, transparent: true, frame: false, alwaysOnTop: true });
	splash.loadURL(`file://${__dirname}/splash.html`);*/
	
	// and load the index.html of the app.
	mainWindow.loadURL(`file://${__dirname}/src/index.html`);

	if (process.env.NODE_ENV == 'development') {
		// Open the DevTools.
		mainWindow.webContents.openDevTools();
	}

	// Emitted when the window is closed.
	mainWindow.on('closed', () => {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		// for(var tab of beApp.tabs.tabs) if(tab.webview && tab.webview.remove) tab.webview.remove();
		mainWindow = null;
	});
	
	// mainWindow.once('ready-to-show', () => {
		// setTimeout(() => {
			// // splash.destroy();
			// mainWindow!.show();
		// }, 25);
	// });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow();
	}
});

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
