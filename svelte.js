const fs = require('fs-extra'),
	path = require('path'),
	glob = require('glob'),
	svelte = require('svelte/compiler');

glob('src/*.svelte', {}, function (er, files) {
	files.forEach(file => {
		const fileContent = fs.readFileSync(file, 'utf8');
		const { js, css } = svelte.compile(fileContent, {
			format: 'cjs',
		});
		let name = path.basename(file, '.svelte');
		fs.writeFileSync('src/' + name + '.svelte.js', js.code);
		fs.writeFileSync('src/' + name + '.svelte.css', css.code);
		console.log(`${name} ok!`);
	});
});
