declare module 'pixi-apngandgif' {
	import { Sprite } from 'pixi.js';

	class PixiApngAndGif {
		sprite: Sprite;

		constructor(imageSource: string, resources: { [key: string]: object })
		/**
		* Play animation boutUsed to specify the number of plays callbackCallback executed after the specified number of plays has been completed.
		*/
		play(bout: string, callback: Function): void;

		/**
		* Pause animation.
		*/
		pause(): void;

		/**
		* Stop animation.
		*/
		stop(): void;

		/**
		* Jump to the specified frame.
		*/
		jumpToFrame(frame: number): void;

		/**
		* Get the total duration of an animation single play.
		*/
		getDuration(): number;

		/**
		* Get the number of animation frames.
		*/
		getFramesLength(): number;

		/**
		* Used to invoke the specified method in the specified phase of the animation statusFour states(playing、played、pause、stop) callbackCallback, there is a parameter. The status of the current animation is recorded.
		*/
		on(status: string, callback: Function): void;
	}
	export = PixiApngAndGif;
}

