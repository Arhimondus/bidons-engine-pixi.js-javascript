interface Object {
	also<T>(callback: (action: T) => any): T;
	let<T, R>(callback: (action: T) => R): R;
}